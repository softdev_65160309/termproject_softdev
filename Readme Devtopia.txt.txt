# Readme for Coffee Devtopia

## Description

Coffee Devtopia เป็นระบบการซื้อขายของร้านกาแฟ การใช้งานที่เป็นมิตร ระบบฟังก์ชั่นที่มีประสิทธิภาพ  ที่จะสามารถทำให้ธุรกิจเกี่ยวกับการซื้อขายอาหาร เติบโตไปได้อย่างเต็มที่ 

## Installation

1. Download the installation package from our website.
2. ดาวน์โหลด Apache NetBean 19 
3. Java JDK 19
4. ทำการติดตั้ง Apache NetBean 19 และ Java JDK 19
5. เมื่อติดตั้งเสร็จ ทำการเปิด Apache NetBean 19

## Usage

1. เปิด Apache NetBean 19
2. ทำการ Run Login.java ใน com.mycompany.softdevtermprojectdecoffee.ui


## Main Features

- หน้าล็อกอิน ทำการใส่ Username และ Password โดยจะมีการตรวจจับว่าเป็นพนักงานเหรือ Manager
- MainMenu จะเป็นหน้าจอแสดง แถบเมนู สินค้าขายดี และ สินค้าเหลือน้อย
- เมื่อผู้ล็อกอินเป็น Manager จะเห็นปุ่มเมนูการเข้าถึงส่วนต่างๆ ทุกปุ่มเพื่อทำการเข้าถึง ในหน้า MainMenu
- เมื่อผู้ล็อกอินเป็น User จะเห็นและเข้าถึงปุ่มได้แค่บางส่วน
- Point Of Sale จะเข้าสู่หน้าการซื้อขายสินค้า
- Member Management จะเป็นหน้าการจัดการลูกค้าที่เป็นสมาชิก
	-สามารถเพิ่มลบแก้ไข
- Product Management จะเป็นหน้าการจัดการสินค้า
	-สามารถเพิ่มลบแก้ไข
- Stock Management จะเป็นหน้าการจัดการวัตถุดิบในคลัง
	-สามารถเพิ่มลบแก้ไข
	-ปุ่ม Order เข้าสู่การจัดการสั่งซื้อวัตุดิบ
	-ปุ่ม Check Stock เข้าสู่การเช็ควัตุดิบคงเหลือ และการสั่งสือวัตถุดิบเหลือน้อย
- Employee Management จะเป็นหน้าการจัดการวัตถุดิบในคลัง
	-สามารถเพิ่มลบแก้ไข
	-ปุ่ม Check Working Time เข้าสู่การจัดการเวลาการทำงานของพนักงาน
	-ปุ่ม Salary จัดการการจ่ายเงินเดือนของพนักงาน
- Bills จะเป็นหน้าการดูรายจ่ายการ
- Music จะเป็นป็อปอัพการจัดการเล่นเสียง
- Logout จะกลับเข้าสู่หน้าล็อคอิน


## Version History

- v1.0 (November 9, 2023): Initial release.

## License

This software is provided under the DEVTOPIA Public License (DPL).
