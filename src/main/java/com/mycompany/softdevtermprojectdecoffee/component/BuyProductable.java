/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.component;

import com.mycompany.softdevtermprojectdecoffee.model.Product;

/**
 *
 * @author Sabusi
 */
public interface BuyProductable {
    public void buy(Product product, int qty);
}
