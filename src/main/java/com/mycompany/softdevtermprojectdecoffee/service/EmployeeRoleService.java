/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.service;

import com.mycompany.softdevtermprojectdecoffee.dao.EmployeeRoleDao;
import com.mycompany.softdevtermprojectdecoffee.model.EmployeeRole;
import java.util.List;

/**
 *
 * @author Raniny
 */
public class EmployeeRoleService {
   
    public List<EmployeeRole> getEmployeeRoles(){
        EmployeeRoleDao employeeroleDao = new EmployeeRoleDao();
        return employeeroleDao.getAll(" EMPLOYEE_ROLE_ID asc");
    }

    public EmployeeRole addNew(EmployeeRole editedEmployeeRole) {
        EmployeeRoleDao employeeroleDao = new EmployeeRoleDao();
        return employeeroleDao.save(editedEmployeeRole);
    }

    public EmployeeRole update(EmployeeRole editedEmployeeRole) {
        EmployeeRoleDao employeeroleDao = new EmployeeRoleDao();
        return employeeroleDao.update(editedEmployeeRole);
    }

    public int delete(EmployeeRole editedEmployeeRole) {
        EmployeeRoleDao employeeroleDao = new EmployeeRoleDao();
        return employeeroleDao.delete(editedEmployeeRole);
    }
}
