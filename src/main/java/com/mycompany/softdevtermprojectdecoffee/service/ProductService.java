/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.service;
import com.mycompany.softdevtermprojectdecoffee.dao.MemberDao;
import com.mycompany.softdevtermprojectdecoffee.model.Product;
import com.mycompany.softdevtermprojectdecoffee.dao.ProductDao;
import com.mycompany.softdevtermprojectdecoffee.model.Member;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Raniny
 */
public class ProductService {
    private ProductDao productDao = new ProductDao();
    public ArrayList<Product> getProductsOrderByName() {
        return (ArrayList<Product>) productDao.getAll(" product_name ASC ");
    }

    public List<Product> getProducts() {
        ProductDao productDao = new ProductDao();
        return productDao.getAll(" PRODUCT_ID asc");
    }

        public int getSizeByCategoryId(int id) {
        ProductDao productDao = new ProductDao();
            return productDao.getSizeByCategoryId(id);
    }

    public Product addNew(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.delete(editedProduct);
    }

}
