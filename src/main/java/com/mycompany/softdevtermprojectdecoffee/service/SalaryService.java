/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.service;

import com.mycompany.softdevtermprojectdecoffee.dao.SalaryDao;
import com.mycompany.softdevtermprojectdecoffee.model.Salary;
import java.util.List;

/**
 *
 * @author HP
 */
public class SalaryService {
    public List<Salary> getSalarys(){
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.getAll(" SALARY_ID asc");
    }
    
    public Salary getSalaryByID(int id){
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.get(id);
    }

    public Salary addNew(Salary editedSalary) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.save(editedSalary);
    }

    public Salary update(Salary editedSalary) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.update(editedSalary);
    }

    public int delete(Salary editedSalary) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.delete(editedSalary);
    }
    
    public List<Salary> getSalarysByEmployeeId(int id) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.getAllByEmp(id);
    }
//    public List<Salary> getSalaryByAmount(){
//        SalaryDao salaryDao = new SalaryDao();
//        return salaryDao.getAll("salary_amount","ASC");
//    }
}
