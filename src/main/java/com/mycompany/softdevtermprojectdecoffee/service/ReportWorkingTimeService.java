/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.service;

import com.mycompany.softdevtermprojectdecoffee.dao.ReportWorkingTimeDao;
import com.mycompany.softdevtermprojectdecoffee.model.ReportWorkingTime;
import java.util.List;

/**
 *
 * @author HP
 */
public class ReportWorkingTimeService {
    public ReportWorkingTime getTotalHourByMonthAndEmp(int id, String month){
        ReportWorkingTimeDao dao = new ReportWorkingTimeDao();
        return dao.get(id, month);
    }
}
