/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.service;

import com.mycompany.softdevtermprojectdecoffee.dao.EmployeeDao;
import com.mycompany.softdevtermprojectdecoffee.model.Employee;
import java.util.List;

/**
 *
 * @author werapan
 */
public class EmployeeService {
    public static Employee currentUser;
    public Employee login(String login, String password) {
        EmployeeDao employeeDao = new EmployeeDao();
        Employee employee = employeeDao.getByLogin(login);
        if(employee != null && employee.getPassword().equals(password)) {
            currentUser = employee;
            return employee;
        }
        return null;
    }
    
    public static Employee getCurrentUser() {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getByLogin(currentUser.getLogin());
    }
    
    public List<Employee> getEmployees(){
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll(" EMPLOYEE_ID asc");
    }

    public Employee addNew(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.save(editedEmployee);
    }

    public Employee update(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.update(editedEmployee);
    }

    public int delete(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.delete(editedEmployee);
    }
    public List<Employee> getName(){
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll(" EMPLOYEE_ID asc");
    }
}