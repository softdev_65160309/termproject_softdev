/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.service;

import com.mycompany.softdevtermprojectdecoffee.model.Category;
import com.mycompany.softdevtermprojectdecoffee.dao.CategoryDao;
import java.util.List;

/**
 *
 * @author Raniny
 */
public class CategoryService {

    public List<Category> getCategorys() {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.getAll(" CATEGORY_ID asc");
    }

    public Category addNew(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.save(editedCategory);
    }

    public Category update(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.update(editedCategory);
    }

    public int delete(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.delete(editedCategory);
    }

}
