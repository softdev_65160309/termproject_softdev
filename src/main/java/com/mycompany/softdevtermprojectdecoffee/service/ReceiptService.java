/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.service;

import com.mycompany.softdevtermprojectdecoffee.dao.ReceiptDao;
import com.mycompany.softdevtermprojectdecoffee.dao.ReceiptDetailDao;
import com.mycompany.softdevtermprojectdecoffee.model.Receipt;
import com.mycompany.softdevtermprojectdecoffee.model.ReceiptDetail;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ReceiptService {
    
    public Receipt getById(int id){
        ReceiptDao recieptDao = new ReceiptDao();
        return recieptDao.get(id);
    }

    public List<Receipt> getReceipt() {
        ReceiptDao recieptDao = new ReceiptDao();
        return recieptDao.getAll(" RECEIPT_ID asc");
    }
    
    public List<ReceiptDetail> getByBestSeller() {
        ReceiptDetailDao recieptDetailDao = new ReceiptDetailDao();
        return recieptDetailDao.getBestSeller();
    }

    public Receipt addNew(Receipt editedReceipt) {
        ReceiptDao recieptDao = new ReceiptDao();
        ReceiptDetailDao recieptDetailDao = new ReceiptDetailDao();
        Receipt reciept =  recieptDao.save(editedReceipt);
        for(ReceiptDetail rd: editedReceipt.getReceiptDetails()) {
            rd.setReceiptId(reciept.getId());
            recieptDetailDao.save(rd);
            
        }
        return reciept;
    }

    public Receipt update(Receipt editedReceipt) {
        ReceiptDao recieptDao = new ReceiptDao();
        return recieptDao.update(editedReceipt);
    }

    public int delete(Receipt editedReceipt) {
        ReceiptDao recieptDao = new ReceiptDao();
        return recieptDao.delete(editedReceipt);
    }
}
