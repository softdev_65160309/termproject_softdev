/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.service;

import com.mycompany.softdevtermprojectdecoffee.dao.CheckstockDao;
import com.mycompany.softdevtermprojectdecoffee.dao.CheckstockDetailDao;
import com.mycompany.softdevtermprojectdecoffee.model.Checkstock;
import com.mycompany.softdevtermprojectdecoffee.model.CheckstockDetail;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class CheckstockService {
    public List<Checkstock> getCheckstocks(){
        CheckstockDao checkstockDao = new CheckstockDao();
        return checkstockDao.getAll(" CHECKSTOCK_ID ASC");
    }
    
    public int getTotal(){
        CheckstockDetailDao checkstockDetailDao = new CheckstockDetailDao();
        return checkstockDetailDao.getTotalPrice();
    }
    
    public List<CheckstockDetail> getCheckstockDetails(){
        CheckstockDetailDao checkstockDetailDao = new CheckstockDetailDao();
        return checkstockDetailDao.getAll(" CHECKSTOCK_DETAIL_ID ASC");
    }
    
    public List<CheckstockDetail> getCheckstockDetailsLatest(){
        CheckstockDetailDao checkstockDetailDao = new CheckstockDetailDao();
        return checkstockDetailDao.getLatest(" CHECKSTOCK_DETAIL_ID ASC");
    }
    
    public List<Checkstock> getCheckstock(){
        CheckstockDao checkstockDao = new CheckstockDao();
        return checkstockDao.getAll(" CHECKSTOCK_ID ASC");
    }  
    
    public Checkstock getCheckstockByID(int id){
        CheckstockDao checkstockDao = new CheckstockDao();
        return checkstockDao.get(id);
    }
    
    public int getLatestId() {
        CheckstockDetailDao checkstockDao = new CheckstockDetailDao();
        return checkstockDao.getLatestId();
    }

    public CheckstockDetail addNewdetail(CheckstockDetail editedCheckstock) {
        CheckstockDetailDao checkstockdetailDao = new CheckstockDetailDao();
        return checkstockdetailDao.save(editedCheckstock);
    }
    
    public Checkstock addNew(Checkstock editedCheckstock) {
        CheckstockDao checkstockDao = new CheckstockDao();
        CheckstockDetailDao checkstockdetailDao = new CheckstockDetailDao();
        Checkstock checkstock = checkstockDao.save(editedCheckstock);
        for (CheckstockDetail cd: editedCheckstock.getCheckstockDetails()){
            cd.setCheckstockId(checkstock.getId());
            checkstockdetailDao.save(cd);
        }
        return checkstock;
    }

    public Checkstock update(Checkstock editedCheckstock) {
        CheckstockDao checkstockDao = new CheckstockDao();
        return checkstockDao.update(editedCheckstock);
    }

    public int delete(Checkstock editedCheckstock) {
        CheckstockDao checkstockDao = new CheckstockDao();
        return checkstockDao.delete(editedCheckstock);
    }
}
