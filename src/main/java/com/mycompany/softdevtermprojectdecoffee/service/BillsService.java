
package com.mycompany.softdevtermprojectdecoffee.service;

import com.mycompany.softdevtermprojectdecoffee.model.Bills;
import com.mycompany.softdevtermprojectdecoffee.dao.BillsDao;
import java.util.ArrayList;
import java.util.List;


public class BillsService {
    

    public Bills getBillsId(int id){
        BillsDao billsDao = new BillsDao();
        return billsDao.get(id);
    }
    
    public List<Bills> getBills() {
        BillsDao BillsDao = new BillsDao();
        return BillsDao.getAll("BILLS_ID asc");
    }

    public Bills addNew(Bills editedBills) {
        BillsDao BillsDao = new BillsDao();
        return BillsDao.save(editedBills);
    }

    public Bills update(Bills editedBills) {
        BillsDao BillsDao = new BillsDao();
        return BillsDao.update(editedBills);
    }

    public int delete(Bills editedBills) {
        BillsDao BillsDao = new BillsDao();
        return BillsDao.delete(editedBills);
    }
}
