package com.mycompany.softdevtermprojectdecoffee.service;

import com.mycompany.softdevtermprojectdecoffee.model.OrderMaterialDetail;
import com.mycompany.softdevtermprojectdecoffee.dao.OrderMaterialDao;
import com.mycompany.softdevtermprojectdecoffee.model.OrderMaterial;
import com.mycompany.softdevtermprojectdecoffee.dao.OrderMaterialDetailDao;
import com.mycompany.softdevtermprojectdecoffee.ui.Order;
import java.util.List;
//import java.util.ArrayList;

public class OrderMaterialService {
//    private ArrayList<OrderMaterial> ordermateriallist;
//
//    public OrderMaterial getOrderMaterial(int index){
//        return ordermateriallist.get(index);
//    }

    public OrderMaterial getById(int id) {
        OrderMaterialDao OrdermaterialDao = new OrderMaterialDao();
        return OrdermaterialDao.get(id);
    }

    public List<OrderMaterial> getOrderMaterial() {
        OrderMaterialDao ordermaterialDao = new OrderMaterialDao();
        return ordermaterialDao.getAll(" ORDER_MATERIAL_ID asc");
    }

    public List<OrderMaterialDetail> getOrderMaterialDetail() {
        OrderMaterialDetailDao ordermaterialdetailDao = new OrderMaterialDetailDao();
        return ordermaterialdetailDao.getAll(" ORDER_MATERIAL_DETAIL_ID asc");
    }

//    public OrderMaterial addNew(OrderMaterial editedOrderMaterial) {
//        OrderMaterialDao promotionDao = new OrderMaterialDao();
//        PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
//       Promotion promotion =  promotionDao.save(editedPromotion);
//       for(PromotionDetail rd: editedPromotion.getPromotionDetails()) {
//            rd.setPromotionId(promotion.getId());
//            promotionDetailDao.save(rd);
//            
//        }
//        return promotion; 
//    }
//    public OrderMaterial addNew(OrderMaterial editedOrderMaterial) {
//        OrderMaterialDao ordermaterialDao = new OrderMaterialDao();
//        OrderMaterialDetailDao ordermaterialDetailDao = new OrderMaterialDetailDao();
//        OrderMaterial ordermaterial =  ordermaterialDao.save(editedOrderMaterial);
//        for(OrderMaterialDetail rd: editedOrderMaterial.getOrderMaterialDetails()) {
//            rd.setOrderMaterialId(ordermaterial.getId());
//            ordermaterialDetailDao.save(rd);
//        }
//        return ordermaterial;
//    }
    public int getLatestId() {
        OrderMaterialDetailDao ordermaterialDetailDao = new OrderMaterialDetailDao();
        return ordermaterialDetailDao.getLatestId();
    }

    public OrderMaterial addNew(OrderMaterial editedOrderMaterial) {
        OrderMaterialDao ordermaterialDao = new OrderMaterialDao();
        return ordermaterialDao.save(editedOrderMaterial);
    }

    public OrderMaterialDetail addNew(OrderMaterialDetail editedOrderMaterialDetail) {
        OrderMaterialDetailDao orderMaterialdetailDao = new OrderMaterialDetailDao();
        return orderMaterialdetailDao.save(editedOrderMaterialDetail);
    }

    public OrderMaterial update(OrderMaterial editedOrderMaterial) {
        OrderMaterialDao ordermaterialDao = new OrderMaterialDao();
        return ordermaterialDao.update(editedOrderMaterial);
    }

    public OrderMaterialDetail updateDetail(OrderMaterialDetail editedOrderMaterialDetail) {
        OrderMaterialDetailDao orderMaterialdetailDao = new OrderMaterialDetailDao();
        return orderMaterialdetailDao.update(editedOrderMaterialDetail);
    }

    public int delete(OrderMaterial editedOrderMaterial) {
        OrderMaterialDao ordermaterialDao = new OrderMaterialDao();
        return ordermaterialDao.delete(editedOrderMaterial);
    }

    public int deleteDetail(OrderMaterialDetail editedOrderMaterialDetail) {
        OrderMaterialDetailDao orderMaterialdetailDao = new OrderMaterialDetailDao();
        return orderMaterialdetailDao.delete(editedOrderMaterialDetail);
    }

//    public List<OrderMaterial> getOrderMaterial() {
//        OrderMaterialDao ordermaterialDao = new OrderMaterialDao();
//        return ordermaterialDao.getAll(" ORDER_MATERIAL_ID ASC");  
//    }
}
