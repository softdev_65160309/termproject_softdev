
package com.mycompany.softdevtermprojectdecoffee.service;

import com.mycompany.softdevtermprojectdecoffee.model.Promotion;
import com.mycompany.softdevtermprojectdecoffee.model.PromotionDetail;
import com.mycompany.softdevtermprojectdecoffee.dao.PromotionDao;
import com.mycompany.softdevtermprojectdecoffee.dao.PromotionDetailDao;
import java.util.List;


public class PromotionService {
    private List<PromotionDetail> list;
    
    
        public Promotion getById(int id){
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.get(id);
    }
         public PromotionDetail getDetailById(int id){
        PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
        return promotionDetailDao.get(id);
    }
        
    public List<Promotion> getPromotion() {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.getAll(" PROMOTION_ID ASC");
    }
    
    public List<PromotionDetail> getPromotionDetail() {
        PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
        return promotionDetailDao.getAll(" PROMOTION_DETAIL_ID ASC");
    }
    
        public PromotionDetail getPromotionDetailById(int id) {
        PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
        return promotionDetailDao.get(id);
    }

    public PromotionDetail getPromotionDetailByCondition(int id) {
        String condition;
        PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
        if (id > 0) {
            condition = "Member";
        } else {
            condition = "None";
        }
        return promotionDetailDao.getByCondition(condition);
    }


    public Promotion addNew(Promotion editedPromotion,PromotionDetail editedPromotionDetail) {
        PromotionDao promotionDao = new PromotionDao();
        PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
       Promotion promotion =  promotionDao.save(editedPromotion);
       PromotionDetail promotionDetail = promotionDetailDao.save(editedPromotionDetail);
       for (PromotionDetail rd : editedPromotion.getPromotionDetails()) {
            rd.setPromotionDetailDetailId(promotion.getId());
            promotionDetailDao.save(rd);
        }
        return promotion;
    }

    public Promotion update(Promotion editedPromotion,PromotionDetail editedPromotionDetail) {
        PromotionDao promotionDao = new PromotionDao();
        PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
        return promotionDao.update(editedPromotion);
    }

    public int delete(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.delete(editedPromotion);
    }

}
