/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.service;

import com.mycompany.softdevtermprojectdecoffee.model.Material;
import com.mycompany.softdevtermprojectdecoffee.dao.MaterialDao;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Raniny
 */
public class MaterialService {
    private ArrayList<Material> materiallist;

    public Material getMaterial(int index){
        return materiallist.get(index);
    }
    
    public Material getMaterial(String name){
        MaterialDao materialDao = new MaterialDao();
        return materialDao.get(" MATERIAL_NAME ");
    }
    
    public List<Material> getMaterials() {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.getAll(" MATERIAL_ID asc");
    }

    public Material addNew(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.save(editedMaterial);
    }

    public Material update(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.update(editedMaterial);
    }

    public int delete(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.delete(editedMaterial);
    }

    public Material updateKeepUnit(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.updateKeepUnit(editedMaterial);
    }

}
