/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.service;

import com.mycompany.softdevtermprojectdecoffee.dao.WorkingTimeDao;
import com.mycompany.softdevtermprojectdecoffee.model.WorkingTime;
import java.util.List;

/**
 *
 * @author supha
 */
public class WorkingTimeService {

    public List<WorkingTime> getWorkingTime() {
        WorkingTimeDao workingtimeDao = new WorkingTimeDao();
        return workingtimeDao.getAll(" WORKINGTIME_ID ASC");
    }

    public WorkingTime addNew(WorkingTime editedWorkingTime) {
        WorkingTimeDao workingtimeDao = new WorkingTimeDao();
        return workingtimeDao.save(editedWorkingTime);
    }

    public WorkingTime update(WorkingTime editedWorkingTime) {
        WorkingTimeDao workingtimeDao = new WorkingTimeDao();
        return workingtimeDao.update(editedWorkingTime);
    }

    public int delete(WorkingTime editedWorkingTime) {
        WorkingTimeDao workingtimeDao = new WorkingTimeDao();
        return workingtimeDao.delete(editedWorkingTime);
    }
    public List<WorkingTime> getEmpId(int id) {
        WorkingTimeDao workingtimeDao = new WorkingTimeDao();
        return  workingtimeDao.getEmp(id);
    }
    
}
