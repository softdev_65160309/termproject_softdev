/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class InsertDatabase {
    public void Insert_Database(int x,int xx,int y,String z,String zz) { // x,y,zz = ค่าที่จะเปลี่ยน // ตัวอย่างค่า z ="INSERT INTO category(category_id, category_name) VALUES (?, ?)"
        Connection conn = null; // ตัวอย่าง x = 1 ,y = 3 , zz = "Candy"
        String url = "jdbc:sqlite:DcoffeeDataBase.db";
        // Connection database
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        // Insert
        String sql = z; // แก้ตัวนี้
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(x, y); //ใส่ข้อมูล
            stmt.setString(xx, zz);
            int status = stmt.executeUpdate();
     //       ResultSet key = stmt.getGeneratedKeys();
   //         key.next();
 //           System.out.println("" + key.getInt(1));
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}

