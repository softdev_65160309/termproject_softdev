/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.model;

import com.mycompany.softdevtermprojectdecoffee.dao.WorkingTimeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author supha
 */
public class WorkingTime {
    private int id;
    private Date checkin;
    private Date checkout;
    private int salaryId;
    private int employeeId;
    private float totalhour;

    public WorkingTime(int id, int salaryId, int employeeId) {
        this.id = id;
        this.salaryId = salaryId;
        this.employeeId = employeeId;
    }

    public WorkingTime(int salaryId, int employeeId) {
        this.id = -1;
        this.salaryId = salaryId;
        this.employeeId = employeeId;
    }

    public WorkingTime() {
        this.id = -1;
        this.checkin = null;
        this.checkout = null;
        this.salaryId = 0;
        this.employeeId = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCheckin() {
        return checkin;
    }

    public void setCheckin(Timestamp checkin) {
        this.checkin = checkin;
    }
    
    public Date getCheckout() {
        return checkout;
    }

    public void setCheckout(Timestamp checkout) {
        this.checkout = checkout;
    }

    public int getSalaryId() {
        return salaryId;
    }
    public void setSalaryId(int salaryId) {
        this.salaryId = salaryId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }
    public float getTotalHour() {
        return totalhour;
    }
    public void setTotalHour(float totalhour) {
        this.totalhour = totalhour;
    }

    @Override
    public String toString() {
        return "WorkingTime{" + "id=" + id + ", checkin=" + checkin + ", checkout=" + checkout + ", salaryId=" + salaryId + ", employeeId=" + employeeId + ", TotalHour=" + totalhour + '}';
    }
    
    public static WorkingTime fromRS(ResultSet rs) { //for receive data and connect to database
        WorkingTime workingtime = new WorkingTime();
        try {
            workingtime.setId(rs.getInt("WORKINGTIME_ID"));
            workingtime.setCheckin(rs.getTimestamp("WORKINGTIME_CHECKIN"));
            workingtime.setCheckout(rs.getTimestamp("WORKINGTIME_CHECKOUT"));
            workingtime.setSalaryId(rs.getInt("SALARY_ID"));
            workingtime.setEmployeeId(rs.getInt("EMPLOYEE_ID"));
            workingtime.setTotalHour(rs.getFloat("WORKINGTIME_TOTALHOUR"));
        } catch (SQLException ex) {
            Logger.getLogger(WorkingTime.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return workingtime;
    }
    
    
    
    
}
