
package com.mycompany.softdevtermprojectdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class OrderMaterialDetail {
    private int ordermaterialdetailId;
    private int ordermaterialdetailUnit;
    private float ordermaterialdetailPrice;
    private int ordermaterialId;
    private int materialId;

    public OrderMaterialDetail(int ordermaterialdetailId, int ordermaterialdetailUnit,float ordermaterialdetailPrice,int ordermaterialId,int materialId) {
        this.ordermaterialdetailId = ordermaterialdetailId;
        this.ordermaterialdetailUnit = ordermaterialdetailUnit;
        this.ordermaterialdetailPrice = ordermaterialdetailPrice;
        this.ordermaterialId = ordermaterialId;
        this.materialId= materialId;
    }

    public OrderMaterialDetail(int ordermaterialdetailUnit,float ordermaterialdetailPrice,int ordermaterialId,int materialId) {
        this.ordermaterialdetailId = -1;
        this.ordermaterialdetailUnit = ordermaterialdetailUnit;
        this.ordermaterialdetailPrice = ordermaterialdetailPrice;
        this.ordermaterialId = ordermaterialId;
        this.materialId= materialId;
    }
    
    public OrderMaterialDetail() {
        this.ordermaterialdetailId = -1;
        this.ordermaterialdetailUnit = 0;
        this.ordermaterialdetailPrice = 0;
        this.ordermaterialId = 0;
        this.materialId= 0;
    }

    public int getOrdermaterialdetailId() {
        return ordermaterialdetailId;
    }

    public void setOrdermaterialdetailId(int ordermaterialdetailId) {
        this.ordermaterialdetailId = ordermaterialdetailId;
    }

    public int getOrdermaterialdetailUnit() {
        return ordermaterialdetailUnit;
    }

    public void setOrdermaterialdetailUnit(int ordermaterialdetailUnit) {
        this.ordermaterialdetailUnit = ordermaterialdetailUnit;
    }

    public float getOrdermaterialdetailPrice() {
        return ordermaterialdetailPrice;
    }

    public void setOrdermaterialdetailPrice(float ordermaterialdetailPrice) {
        this.ordermaterialdetailPrice = ordermaterialdetailPrice;
    }

    public int getOrdermaterialId() {
        return ordermaterialId;
    }

    public void setOrdermaterialId(int ordermaterialId) {
        this.ordermaterialId = ordermaterialId;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }
    

    @Override
    public String toString() {
        return "ORDER_MATERIAL_DETAIL{" + "ORDER_MATERIAL_DETAIL_ID=" + ordermaterialdetailId + ", ORDER_MATERIAL_DETAIL_UNIT=" + ordermaterialdetailUnit + ", ORDER_MATERIAL_DETAIL_PRICE=" + ordermaterialdetailPrice + ",  ORDER_MATERIAL_ID=" + ordermaterialId + ", MATERIAL_ID=" + materialId + '}';
    }
    public static OrderMaterialDetail fromRS(ResultSet rs) {
        OrderMaterialDetail ordermaterialDetail = new OrderMaterialDetail();
        try {
            ordermaterialDetail.setOrdermaterialdetailId(rs.getInt("ORDER_MATERIAL_DETAIL_ID"));
            ordermaterialDetail.setOrdermaterialdetailUnit(rs.getInt("ORDER_MATERIAL_DETAIL_UNIT"));
            ordermaterialDetail.setOrdermaterialdetailPrice(rs.getFloat("ORDER_MATERIAL_DETAIL_PRICE"));
            ordermaterialDetail.setOrdermaterialId(rs.getInt("ORDER_MATERIAL_ID"));
            ordermaterialDetail.setMaterialId(rs.getInt("MATERIAL_ID"));
        } catch (SQLException ex) {
            Logger.getLogger(OrderMaterialDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return ordermaterialDetail;
    }

    public void setOrderMaterialId(int id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
