
package com.mycompany.softdevtermprojectdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class PromotionDetail {
    private int promotionId;
    private int detailId;
    private float discount;
    private String descript;
    private String condition;
    private String status;

    public PromotionDetail(int promotionId, int detailId,float discount,String descript,String condition,String status) {
        this.promotionId = promotionId;
        this.detailId = detailId;
        this.discount = discount;
        this.status = status;
        this.descript= descript;
        this.condition = condition;
    }

    public PromotionDetail(String descript) {
        this.detailId = -1;
        this.descript= descript;
    }
    
    public PromotionDetail() {
        this.promotionId = -1;
        this.descript = "";
    }

    public int getPromotionDetailDetailId() {
        return detailId;
    }

    public void setPromotionDetailDetailId(int detailId) {
        this.detailId = detailId;
    }

    public float getPromotionDetailDiscount() {
        return discount;
    }

    public void setPromotionDetailDiscount(float discount) {
        this.discount = discount;
    }
    
    public String getPromotionDetailDescript() {
        return descript;
    }

    public void setPromotionDetailDescript(String descript) {
        this.descript = descript;
    }
    
    public String getPromotionDetailCondition() {
        return condition;
    }

    public void setPromotionDetailCondition(String condition) {
        this.condition = condition;
    }
    
    public String getPromotionDetailStatus() {
        return status;
    }

    public void setPromotionDetailStatus(String status) {
        this.status = status;
    }
    
    public int getPromotionDetailPromotionId() {
        return promotionId;
    }

    public void setPromotionDetailPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    @Override
    public String toString() {
        return "PROMOTION_DETAIL{" + "PROMOTION_ID=" + promotionId + ", PROMOTION_DETAIL_ID=" + detailId + ", PROMOTION_DETAIL_DISCOUNT="+ discount +", POMOTION_DETAIL_DESCRIPT=" + descript +", PROMOTION_DETAIL_STATUS=" + status +", PROMOTION_DETAIL_CONDITION=" + condition +'}';
    }
    
    public static PromotionDetail fromRS(ResultSet rs) {
        PromotionDetail promotionDetail = new PromotionDetail();
        try {
            promotionDetail.setPromotionDetailDetailId(rs.getInt("PROMOTION_DETAIL_ID"));
            promotionDetail.setPromotionDetailDescript(rs.getString("PROMOTION_DETAIL_DESCRIPT"));
            promotionDetail.setPromotionDetailPromotionId(rs.getInt("PROMOTION_ID"));
            promotionDetail.setPromotionDetailCondition(rs.getString("PROMOTION_DETAIL_CONDITION"));
            promotionDetail.setPromotionDetailDiscount(rs.getFloat("PROMOTION_DETAIL_DISCOUNT"));
            promotionDetail.setPromotionDetailStatus(rs.getString("PROMOTION_DETAIL_STATUS"));
        } catch (SQLException ex) {
            Logger.getLogger(PromotionDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotionDetail;
    }

    public void setPromotionId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
