package com.mycompany.softdevtermprojectdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author Sabusi
 */
public class ReceiptDetail {

    private int id;
    private int productId;
    private String productName;
    private float productPrice;
    private int qty;
    private float totalPrice;
    private int recieptId;
    private int promotionId;

    public ReceiptDetail(int id, int productId, String productName, float productPrice, int qty, float totalPrice, int recieptId, int promotionId) {
        this.id = id;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptId = recieptId;
        this.promotionId = promotionId;
    }
    
        public ReceiptDetail(int productId, String productName, float productPrice, int qty, float totalPrice, int recieptId, int promotionId) {
        this.id = -1;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptId = recieptId;
        this.promotionId = promotionId;
    }
    
    public ReceiptDetail(){
        this.id = -1;
        this.productId = 0;
        this.productName = null;
        this.productPrice = 0;
        this.qty = 0;
        this.totalPrice = 0;
        this.recieptId = 0;
        this.promotionId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
        totalPrice = qty * productPrice;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getReceiptId() {
        return recieptId;
    }

    public void setReceiptId(int recieptId) {
        this.recieptId = recieptId;
    }

    public int getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    @Override
    public String toString() {
        return "ReceiptDetail{" + "id=" + id + ", productId=" + productId + ", productName=" + productName + ", productPrice=" + productPrice + ", qty=" + qty + ", totalPrice=" + totalPrice + ", recieptId=" + recieptId + ", promotionId=" + promotionId + '}';
    }

    public static ReceiptDetail fromRS(ResultSet rs) {
        ReceiptDetail recieptDetial = new ReceiptDetail();
        try {
            recieptDetial.setId(rs.getInt("RECEIPT_DETAIL_ID"));
            recieptDetial.setProductId(rs.getInt("PRODUCT_ID"));
            recieptDetial.setProductName(rs.getString("RECEIPT_DETAIL_NAME"));
            recieptDetial.setProductPrice(rs.getFloat("RECEIPT_DETAIL_PRODUCT_PRICE"));
            recieptDetial.setQty(rs.getInt("RECEIPT_DETAIL_QUANTITY"));
            recieptDetial.setTotalPrice(rs.getFloat("RECEIPT_DETAIL_TOTALPRICE"));
            recieptDetial.setReceiptId(rs.getInt("RECEIPT_ID"));
            recieptDetial.setPromotionId(rs.getInt("PROMOTION_DETAIL_ID"));
            
        } catch (SQLException ex) {
            Logger.getLogger(ReceiptDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return recieptDetial;
    }
    
}
