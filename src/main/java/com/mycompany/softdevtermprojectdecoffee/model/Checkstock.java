/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.model;

import com.mycompany.softdevtermprojectdecoffee.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Checkstock {
    private int id;
    private int totalUnit;
    private Date dateTime;
    private Employee employee;
    private int empId;
    private ArrayList<CheckstockDetail> CheckstockDetails = new ArrayList();

    public Checkstock(int id, int totalUnit, Date dateTime, Employee employee, int empId ) {
        this.id = id;
        this.totalUnit = totalUnit;
        this.dateTime = dateTime;
        this.employee = employee;
        this.empId = empId;
    }
    
    public Checkstock(int totalUnit, Date dateTime, Employee employee, int empId) {
        this.id = -1;
        this.totalUnit = totalUnit;
        this.dateTime = dateTime;
        this.employee = employee;
        this.empId = empId;
    }
    
    public Checkstock(int totalUnit, Employee employee, int empId) {
        this.id = -1;
        this.totalUnit = totalUnit;
        this.dateTime = dateTime;
        this.employee = employee;
        this.empId = empId;
    }
    
    public Checkstock() {
        this.id = -1;
        this.totalUnit = 0;
        this.dateTime = null;
        this.employee = null;
        this.empId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTotalUnit() {
        return totalUnit;
    }

    public ArrayList<CheckstockDetail> getCheckstockDetails() {
        return CheckstockDetails;
    }

    public void setCheckstockDetails(ArrayList<CheckstockDetail> CheckstockDetails) {
        this.CheckstockDetails = CheckstockDetails;
    }

    public void setTotalUnit(int totalUnit) {
        this.totalUnit = totalUnit;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.empId = employee.getId();
        
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    @Override
    public String toString() {
        return "Checkstock{" + "id=" + id + ", totalUnit=" + totalUnit + ", dateTime=" + dateTime + ", employee=" + employee + ", empId=" + empId + '}';
    }
    
    public static Checkstock fromRS(ResultSet rs) { //for receive data and connect to database
        Checkstock checkstock = new Checkstock();
        try {
            checkstock.setId(rs.getInt("CHECKSTOCK_ID"));
            checkstock.setTotalUnit(rs.getInt("CHECKSTOCK_TOTAL_PRICE"));
            checkstock.setDateTime(rs.getTimestamp("CHECKSTOCK_DATETIME"));
            checkstock.setEmpId(rs.getInt("EMPLOYEE_ID"));
            
            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.get(checkstock.getEmpId());
            checkstock.setEmployee(employee);
        } catch (SQLException ex) {
            Logger.getLogger(Checkstock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkstock;
    }
    
    
}
