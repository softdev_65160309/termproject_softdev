/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Raniny
 */
public class EmployeeRole {
    private int id;
    private String name;

    public EmployeeRole(int id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public EmployeeRole(String name) {
        this.id = -1;
        this.name = name;
    }
    
    public EmployeeRole() {
        this.id = -1;
        this.name = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "EmployeeRole{" + "id=" + id + ", name=" + name + '}';
    }
    
    public static EmployeeRole fromRS(ResultSet rs) {
        EmployeeRole employeerole = new EmployeeRole();
        try {
            employeerole.setId(rs.getInt("EMPLOYEE_ROLE_ID"));
            employeerole.setName(rs.getString("EMPLOYEE_ROLE_NAME"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employeerole ;
    }
    
    
}
