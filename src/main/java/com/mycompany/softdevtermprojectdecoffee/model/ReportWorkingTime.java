/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class ReportWorkingTime {
//    private int employeeId;
    private int time_difference;
    private String month;

    public ReportWorkingTime(int time_difference, String month) {
//        this.employeeId = employeeId;
        this.time_difference = time_difference;
        this.month = month;
    }

//    public ReportWorkingTime(int time_difference, String month) {
////        this.employeeId = -1;
//        this.time_difference = time_difference;
//        this.month = month;
//    }
    
    public ReportWorkingTime(){
        this(-1,"");
    }

//    public int getEmployeeId() {
//        return employeeId;
//    }
//
//    public void setEmployeeId(int employeeId) {
//        this.employeeId = employeeId;
//    }

    public int getTime_difference() {
        return time_difference;
    }

    public void setTime_difference(int time_difference) {
        this.time_difference = time_difference;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    @Override
    public String toString() {
        return "ReportWorkingTime{" + " time_difference=" + time_difference + ", month=" + month + '}';
    }
    public static ReportWorkingTime fromRS(ResultSet rs) { //for receive data and connect to database
        ReportWorkingTime reportworkingtime = new ReportWorkingTime();
        try {
//            reportworkingtime.setEmployeeId(rs.getInt("EMPLOYEE_ID"));
            reportworkingtime.setTime_difference(rs.getInt("time_difference"));
            reportworkingtime.setMonth(rs.getString("month"));
        } catch (SQLException ex) {
            Logger.getLogger(WorkingTime.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reportworkingtime;
    }
}
