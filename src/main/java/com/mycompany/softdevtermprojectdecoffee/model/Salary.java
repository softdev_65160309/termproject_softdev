/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.model;

import com.mycompany.softdevtermprojectdecoffee.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class Salary {
    private int id;
    private double amount;
    private Date givetime;
    private String paymentmethod;
    private int employeeId;
    private Employee employee;

    public Salary(int id, double amount, String paymentmethod, int employeeId) {
        this.id = id;
        this.amount = amount;
        this.paymentmethod = paymentmethod;
        this.employeeId = employeeId;
    }

    public Salary(double amount, String paymentmethod, int employeeId) {
        this.id = -1;
        this.amount = amount;
        this.paymentmethod = paymentmethod;
        this.employeeId = employeeId;
    }

    public Salary() {
        this.id = -1;
        this.amount = 0.0;
        this.givetime = null;
        this.paymentmethod = "";
        this.employeeId = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getGivetime() {
        return givetime;
    }

    public void setGivetime(Date givetime) {
        this.givetime = givetime;
    }

    public String getPaymentmethod() {
        return paymentmethod;
    }

    public void setPaymentmethod(String paymentmethod) {
        this.paymentmethod = paymentmethod;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.employeeId = employee.getId();
    }

    @Override
    public String toString() {
        return "Salary{" + "id=" + id + ", amount=" + amount + ", givetime=" + givetime + ", paymentmethod=" + paymentmethod + ", employeeId=" + employeeId + ", employee=" + employee + '}';
    }
    
    public static Salary fromRS(ResultSet rs) { //for receive data and connect to database
        Salary salary = new Salary();
        try {
            salary.setId(rs.getInt("SALARY_ID"));
            salary.setAmount(rs.getDouble("SALARY_AMOUNT"));
            salary.setGivetime(rs.getTimestamp("SALARY_GIVETIME"));
            salary.setPaymentmethod(rs.getString("SALARY_PAYMENT_METHOD"));
            salary.setEmployeeId(rs.getInt("EMPLOYEE_ID"));
            
            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.get(salary.getEmployeeId());
            salary.setEmployee(employee);
        } catch (SQLException ex) {
            Logger.getLogger(Salary.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salary;
    }
    
    
    
    
}
