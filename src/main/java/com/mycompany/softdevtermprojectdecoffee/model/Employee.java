/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class Employee {
    private int id;
    private String login;
    private String name;
    private String password;
    private int role;
    private String gender;
    private String tel;
    private String Address;
    private String Type;

    public Employee(int id, String login, String name, String password, int role, String gender, String tel, String Address, String Type) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.password = password;
        this.role = role;
        this.gender = gender;
        this.tel = tel;
        this.Address = Address;
        this.Type = Type;
    }
    
        public Employee(String login, String name, String password, int role, String gender, String tel, String Address, String Type) {
        this.id = -1;
        this.login = login;
        this.name = name;
        this.password = password;
        this.role = role;
        this.gender = gender;
        this.tel = tel;
        this.Address = Address;
        this.Type = Type;
    }
        public Employee() {
        this.id = -1;
        this.login = "";
        this.name = "";
        this.password =  "";
        this.role = 0;
        this.gender =  "";
        this.tel =  "";
        this.Address =  "";
        this.Type =  "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", login=" + login + ", name=" + name + ", password=" + password + ", role=" + role + ", gender=" + gender + ", tel=" + tel + ", Address=" + Address + ", Type=" + Type + '}';
    }

    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("EMPLOYEE_ID"));
            employee.setLogin(rs.getString("EMPLOYEE_LOGIN"));
            employee.setName(rs.getString("EMPLOYEE_NAME"));
            employee.setRole(rs.getInt("EMPLOYEE_ROLE_ID"));
            employee.setGender(rs.getString("EMPLOYEE_GENDER"));
            employee.setPassword(rs.getString("EMPLOYEE_PASSWORD"));
            employee.setTel(rs.getString("EMPLOYEE_TEL"));
            employee.setAddress(rs.getString("EMPLOYEE_ADDRESS"));
            employee.setType(rs.getString("EMPLOYEE_TYPE"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }
}
