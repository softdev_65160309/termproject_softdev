/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.model;

import com.mycompany.softdevtermprojectdecoffee.dao.EmployeeDao;
import com.mycompany.softdevtermprojectdecoffee.dao.MemberDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class Receipt {

    private int id;
    private Date createdDate;
    private float total;
    private float cash;
    private int point;
    private int userId;
    private int customerId;
    private int promotionDetailId;
    private Employee user;
    private Member customer;
    
   
    private ArrayList<ReceiptDetail> recieptDetails = new ArrayList<ReceiptDetail>();

    public Receipt(int id, Date createdDate, float total, float cash, int point, int userId, int customerId, int promotionDetailId) {
        this.id = id;
        this.createdDate = createdDate;
        this.total = total;
        this.cash = cash;
        this.point = point;
        this.userId = userId;
        this.customerId = customerId;
        this.promotionDetailId = promotionDetailId;
    }
    
    public Receipt(int id, Date createdDate, float total, float cash, int point) {
        this.id = id;
        this.createdDate = createdDate;
        this.total = total;
        this.cash = cash;
        this.point = point;

    }


    public Receipt(float total, float cash, int point, int userId, int customerId, int promotionDetailId) {
        this.id = -1;
        this.createdDate = null;
        this.total = total;
        this.cash = cash;
        this.point = point;
        this.userId = userId;
        this.customerId = customerId;
        this.promotionDetailId = promotionDetailId;
    }

    public Receipt(float cash, int userId, int customerId, int promotionDetailId) {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.cash = cash;
        this.point = 0;
        this.userId = userId;
        this.customerId = customerId;
        this.promotionDetailId = promotionDetailId;
    }

    public Receipt() {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.cash = 0;
        this.userId = 0;
        this.customerId = 0;
        this.promotionDetailId = 0;
    }

//    public Receipt(int id, String name, String tel) {
//        this.id = id;
//        this.name = name;
//        this.tel = tel;
//    }
//
//    public Receipt(String name, String tel) {
//        this.id = -1;
//        this.name = name;
//        this.tel = tel;
//    }
//    
//    
//
//    public Receipt() {
//        this.id = -1;
//        this.name = "";
//        this.tel = "";
//    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public int getEmployeeId() {
        return userId;
    }

    public void setEmployeeId(int userId) {
        this.userId = userId;

    }

    public int getMemberId() {
        return customerId;
    }

    public void setMemberId(int customerId) {
        this.customerId = customerId;
    }

    public Employee getEmployee() {
        return user;
    }

    public void setEmployee(Employee user) {
        this.user = user;
        this.userId = user.getId();
    }

    public Member getMember() {
        return customer;
    }

    public void setMember(Member customer) {
        if(customer != null){
            this.customer = customer;
            this.customerId = customer.getId();
        }else{
            this.customerId = 0;
        }    
    }

    public ArrayList<ReceiptDetail> getReceiptDetails() {
        return recieptDetails;
    }

    public void setReceiptDetails(ArrayList recieptDetails) {
        this.recieptDetails = recieptDetails;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getPromotionDetailId() {
        return promotionDetailId;
    }

    public void setPromotionDetailId(int promotionDetailId) {
        this.promotionDetailId = promotionDetailId;
    }
    
    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", createdDate=" + createdDate + ", total=" + total + ", cash=" + cash + ", point=" + point + ", userId=" + userId + ", customerId=" + customerId + ", promotionDetailId=" + promotionDetailId + ", user=" + user + ", customer=" + customer + ", recieptDetails=" + recieptDetails + '}';
    }

    public void addReceiptDetail(ReceiptDetail recieptDetail) {
        recieptDetails.add(recieptDetail);
        calculateTotal();
    }

    public void addReceiptDetail(Product product, int qty) {
        ReceiptDetail rd = new ReceiptDetail(product.getId(), product.getName(), product.getPrice(), qty, qty*product.getPrice(), -1,-1);
        recieptDetails.add(rd);
        calculateTotal();
    }

    public void delReceiptDetail(ReceiptDetail recieptDetail) {
        recieptDetails.remove(recieptDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for (ReceiptDetail rd : recieptDetails) {
            total += rd.getTotalPrice();
            totalQty += rd.getQty();
        }
        this.total = total;
    }

    public static Receipt fromRS(ResultSet rs) {
        Receipt reciept = new Receipt();
        try {
            reciept.setId(rs.getInt("RECEIPT_ID"));
            reciept.setCreatedDate(rs.getTimestamp("RECEIPT_CREATE_DATE"));
            reciept.setTotal(rs.getFloat("RECEIPT_TOTAL"));
            reciept.setCash(rs.getFloat("RECEIPT_CASH"));
            reciept.setPoint(rs.getInt("RECEIPT_GETPOINT"));
            reciept.setEmployeeId(rs.getInt("EMPLOYEE_ID"));
            reciept.setMemberId(rs.getInt("MEMBER_ID"));
            reciept.setPromotionDetailId(rs.getInt("PROMOTION_DETAIL_ID"));

            // Population
            MemberDao customerDao = new MemberDao();
            EmployeeDao userDao = new EmployeeDao();
            Member customer = customerDao.get(reciept.getMemberId());
            Employee user = userDao.get(reciept.getEmployeeId());
            reciept.setMember(customer);
            reciept.setEmployee(user);

        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciept;
    }
}
