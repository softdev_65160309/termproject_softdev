/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class CheckstockDetail {
    private int id;
    private int unit;
    private int checkstockId;
    private int materialId;
    private float price;

    public CheckstockDetail(int id, int unit, int checkstockId, int materialId, float price) {
        this.id = id;
        this.unit = unit;
        this.checkstockId = checkstockId;
        this.materialId = materialId;
        this.price = price;
    }
    
    public CheckstockDetail(int unit, int checkstockId, int materialId, float price) {
        this.id = -1;
        this.unit = unit;
        this.checkstockId = checkstockId;
        this.materialId = materialId;
        this.price = price;
    }
    
    public CheckstockDetail() {
        this.id = -1;
        this.unit = 0;
        this.checkstockId = 0;
        this.materialId = 0;
        this.price = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public int getCheckstockId() {
        return checkstockId;
    }

    public void setCheckstockId(int checkstockId) {
        this.checkstockId = checkstockId;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }
    
    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "CheckstockDetail{" + "id=" + id + ", unit=" + unit + ", checkstockId=" + checkstockId + ", materialId=" + materialId + ", price=" + price + '}';
    }
    
    public static CheckstockDetail fromRS(ResultSet rs) { //for receive data and connect to database
        CheckstockDetail checkstockDetail = new CheckstockDetail();
        try {
            checkstockDetail.setId(rs.getInt("CHECKSTOCK_DETAIL_ID"));
            checkstockDetail.setUnit(rs.getInt("CHECKSTOCK_DETAIL_UNIT"));
            checkstockDetail.setCheckstockId(rs.getInt("CHECKSTOCK_ID"));
            checkstockDetail.setMaterialId(rs.getInt("MATERIAL_ID"));
            checkstockDetail.setPrice(rs.getFloat("CHECKSTOCK_DETAIL_PRICE"));
           } catch (SQLException ex) {
            Logger.getLogger(CheckstockDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkstockDetail;
    }
    
}
