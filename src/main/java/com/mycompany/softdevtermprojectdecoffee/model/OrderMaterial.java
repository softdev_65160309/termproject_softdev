package com.mycompany.softdevtermprojectdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;

public class OrderMaterial {

    private int id;
    private Date createdDate;
    private float totalprice;
    private int emid;
    private ArrayList<OrderMaterialDetail> ordermaterialDetails = new ArrayList<OrderMaterialDetail>(); 
    
    
    public OrderMaterial(float totalprice, int emid) {
        this.totalprice = totalprice;
        this.emid = emid;
    }
    

    public OrderMaterial(int id, Date createdDate, float totalprice, int emid) {
        this.id = id;
        this.createdDate = createdDate;
        this.totalprice = totalprice;
        this.emid = emid;
    }

    public OrderMaterial(Date createdDate, float totalprice, int emid) {
        this.id = -1;
        this.createdDate = createdDate;
        this.totalprice = totalprice;
        this.emid = emid;
    }

    public OrderMaterial() {
        this.id = -1;
        this.createdDate = null;
        this.totalprice = 0;
        this.emid = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public float getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(float totalprice) {
        this.totalprice = totalprice;
    }

    public int getEmid() {
        return emid;
    }

    public void setEmid(int emid) {
        this.emid = emid;
    }
    
    public ArrayList<OrderMaterialDetail> getOrderMaterialDetails() {
    return ordermaterialDetails;
    }
    public void setOrderMaterialDetails(ArrayList ordermaterialDetails) {
        this.ordermaterialDetails = ordermaterialDetails;
    }

    @Override
    public String toString() {
        return "ORDER_MATERIAL{" + "ORDER_MATERIAL_ID=" + id + ", ORDER_MATERIAL_DATETIME=" + createdDate + ", ORDER_MATERIAL_TOTAL_PRICE=" + totalprice + ", EMPLOYEE_ID=" + emid +'}';
    }
    
    public static OrderMaterial fromRS(ResultSet rs) {
        OrderMaterial ordermaterial = new OrderMaterial();
        try {
            ordermaterial.setId(rs.getInt("ORDER_MATERIAL_ID"));
            ordermaterial.setCreatedDate(rs.getDate("ORDER_MATERIAL_DATETIME"));
            ordermaterial.setTotalprice(rs.getFloat("ORDER_MATERIAL_TOTAL_PRICE"));
            ordermaterial.setEmid(rs.getInt("EMPLOYEE_ID"));
        } catch (SQLException ex) {
            Logger.getLogger(OrderMaterial.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return ordermaterial;
    }

}
