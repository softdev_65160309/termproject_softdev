
package com.mycompany.softdevtermprojectdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;


public class Promotion {
    private int id;
    private int promotionId;
    private String startDate;
    private String endDate;
    private String status;
    private String name;
    private String description;

    public Promotion(int promotionId, String startDate,String endDate,String status,String name,String description) {
        this.promotionId = promotionId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.name = name;
        this.description = description;
    }

    public Promotion(String promotionName) {
        this.promotionId = -1;
        this.name= promotionName;
    }
    
    public Promotion() {
        this.promotionId = -1;
        this.name = "";
    }

        public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    public String getPromotionName() {
        return name;
    }

    public void setPromotionName(String promotionName) {
        this.name = promotionName;
    }
    
    public String getPromotionstartDate() {
        return startDate;
    }

    public void setPromotionstartDate(String startDate) {
        this.startDate = startDate;
    }
    
    public String getPromotionendDate() {
        return endDate;
    }

    public void setPromotionendDate(String endDate) {
        this.endDate = endDate;
    }
    
    public String getPromotionStatus() {
        return status;
    }

    public void setPromotionStatus(String status) {
        this.status = status;
    }
    
    public String getPromotionDescription() {
        return description;
    }

    public void setPromotionDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "PROMOTION{" + "PROMOTION_ID=" + promotionId + ", PROMOTION_NAME=" + name + ", PROMOTION_STARTDATE="+ startDate +", POMOTION_ENDDATE=" + endDate +", PROMOTION_STATUS=" + status +", PROMOTION_DESCRIPTION=" + description +'}';
    }
    
    public static Promotion fromRS(ResultSet rs) {
        Promotion promotion = new Promotion();
        try {
            promotion.setPromotionId(rs.getInt("PROMOTION_ID"));
            promotion.setPromotionName(rs.getString("PROMOTION_NAME"));
            promotion.setPromotionstartDate(rs.getString("PROMOTION_STARTDATE"));
            promotion.setPromotionendDate(rs.getString("PROMOTION_ENDDATE"));
            promotion.setPromotionStatus(rs.getString("PROMOTION_STATUS"));
            promotion.setPromotionDescription(rs.getString("PROMOTION_DESCRIPTION"));
        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotion;
    }

    public Iterable<PromotionDetail> getPromotionDetails() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    
}
