/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Raniny
 */
public class Material {

    private int id;
    private String name;
    private float price;
    private int unit;
    private String unitType;
    private int minimum;

    public Material(int id, String name, float price, int unit, String unitType, int minimum) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.unitType = unitType;
        this.minimum = minimum;
    }

    public Material(String name, float price, int unit, String unitType, int minimum) {
        this.id = -1;
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.unitType = unitType;
        this.minimum = minimum;
    }

    public Material() {
        this.id = -1;
        this.name = "";
        this.price = 0;
        this.unit = 0;
        this.unitType = "";
        this.minimum = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public int getMinimum() {
        return minimum;
    }

    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    @Override
    public String toString() {
        return "Material{" + "id=" + id + ", name=" + name + ", price=" + price + ", unit=" + unit + ", unitType=" + unitType + ", minimum=" + minimum + '}';
    }

    public static Material fromRS(ResultSet rs) {
        Material material = new Material();
        try {
            material.setId(rs.getInt("MATERIAL_ID"));
            material.setName(rs.getString("MATERIAL_NAME"));
            material.setPrice(rs.getFloat("MATERIAL_PRICE"));
            material.setUnit(rs.getInt("MATERIAL_UNIT"));
            material.setUnitType(rs.getString("MATERIAL_UNITTYPE"));
            material.setMinimum(rs.getInt("MATERIAL_MINIMUM"));
        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return material;
    }

}
