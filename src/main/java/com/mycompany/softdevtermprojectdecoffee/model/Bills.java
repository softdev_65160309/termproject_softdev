
package com.mycompany.softdevtermprojectdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Bills {
    private int id;
    private float water;
    private float electric;
    private String paydate;
    private float wtb;
    private float etb;
    private float vat;
    private float usedwater;
    private float usedelectric;

    public Bills(int id,float water,float electric,String paydate) {
        this.id = id;
        this.water = water;
        this.electric = electric;
        this.paydate = paydate;
    }
    
    public Bills(float water,float electric,String paydate) {
        this.id = -1;
        this.water = water;
        this.electric = electric;
        this.paydate = paydate;
    }
    
    public Bills() {
        this.id = -1;
        this.water = 0;
        this.paydate= null;
        this.electric = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getWater() {
        return water;
    }

    public void setWater(float water) {
        this.water = water;
    }

    public String getPaydate() {
        return paydate;
    }

    public void setPaydate(String paydate) {
        this.paydate= paydate;
    }
    
    public float getElectric() {
        return electric;
    }

    public void setElectric(float electric) {
        this.electric = electric;
    }
    
        public float getWtb() {
        return wtb;
    }

    public void setWtb(float wtb) {
        this.wtb = wtb;
    }
    
        public float getEtb() {
        return etb;
    }

    public void setEtb(float etb) {
        this.etb = etb;
    }
    
            public float getVat() {
        return vat;
    }

    public void setVat(float vat) {
        this.vat = vat;
    }
    
            public float getUsedwater() {
        return usedwater;
    }

    public void setUsedwater(float usedwater) {
        this.usedwater= usedwater;
    }
    
    public float getUsedelectric() {
        return usedelectric;
    }

    public void setUsedelectric(float usedelectric) {
        this.usedelectric = usedelectric;
    }

    @Override
    public String toString() {
        return "Bills{" + "id=" + id + ", water=" + water + ", electric=" + electric + ", paydate=" + paydate+",wtb=" + wtb +",etb="+ etb +",vat="+vat+",usedwater="+usedwater +",usedelectric="+usedelectric+'}';
    }
    
    public static Bills fromRS(ResultSet rs) {
        Bills b = new Bills();
        try {
            b.setId(rs.getInt("BILLS_ID"));
            b.setWater(rs.getFloat("BILLS_WATER"));
            b.setElectric(rs.getFloat("BILLS_ELECTRIC"));
            b.setPaydate(rs.getString("BILLS_PAYDATE"));
            b.setWtb(rs.getFloat("BILLS_WTB"));
            b.setEtb(rs.getFloat("BILLS_ETB"));
            b.setVat(rs.getFloat("BILLS_VAT"));
            b.setUsedwater(rs.getFloat("BILLS_USEDWATER"));
            b.setUsedelectric(rs.getFloat("BILLS_USEDELECTRIC"));
        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return b;
    }
    
    
}
