/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sabusi
 */
public class Member {

    private int id;
    private String name;
    private String tel;
    private int point;
    private String startdate;

    public Member(int id, String name, String tel, int point, String startdate) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.point = point;
        this.startdate = startdate;
    }

    public Member(String name, String tel, int point, String startdate) {
        this.id = -1;
        this.name = name;
        this.tel = tel;
        this.point = point;
        this.startdate = startdate;
    }

    public Member() {
        this.id = -1;
        this.name = "";
        this.tel = "";
        this.point = 0;
        this.startdate = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    @Override
    public String toString() {
        return "Member{" + "id=" + id + ", name=" + name + ", tel=" + tel + ", point=" + point + ", startdate=" + startdate + '}';
    }

    public static Member fromRS(ResultSet rs) {
        Member member = new Member();
        try {
            member.setId(rs.getInt("MEMBER_ID"));
            member.setName(rs.getString("MEMBER_NAME"));
            member.setTel(rs.getString("MEMBER_TEL"));
            member.setPoint(rs.getInt("MEMBER_POINT"));
            member.setStartdate(rs.getString("MEMBER_START"));
        } catch (SQLException ex) {
            Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return member;
    }
}
