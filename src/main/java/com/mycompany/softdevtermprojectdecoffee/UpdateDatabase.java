/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class UpdateDatabase {
    public void Update_Database(int x,String y,int xx,int yy,String z) { // x,y = ค่าที่จะเปลี่ยน // ตัวอย่างค่า z ="DELETE FROM category WHERE category_id=?"
                Connection conn = null;                                                     // x,y =(1, "MyCoffee") ,xx,yy = (2, 1)
        String url = "jdbc:sqlite:dcoffee.db";
        // Connection database
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }


        String sql = z; // แก้ตัวนี้
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(x, y); // แก้ตัวนี้
            stmt.setInt(xx, yy); // แก้ตัวนี้
            
            int status = stmt.executeUpdate();
//            ResultSet key = stmt.getGeneratedKeys();
//            key.next();
//            System.out.println("" + key.getInt(1));
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        // Close Database
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
