
package com.mycompany.softdevtermprojectdecoffee.dao;
import com.mycompany.softdevtermprojectdecoffee.helper.ConnectDatabaseHelper;
import com.mycompany.softdevtermprojectdecoffee.model.OrderMaterialDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class OrderMaterialDetailDao implements Dao<OrderMaterialDetail>{

    @Override
    public OrderMaterialDetail get(int id) {
        OrderMaterialDetail ordermaterialDetail = null;
        String sql = "SELECT * FROM ORDER_MATERIAL_DETAIL WHERE ORDER_MATERIAL_DETAIL_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ordermaterialDetail = ordermaterialDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return ordermaterialDetail;
    }

    public List<OrderMaterialDetail> getAll() {
        ArrayList<OrderMaterialDetail> list = new ArrayList();
        String sql = "SELECT * FROM ORDER_MATERIAL_DETAIL";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderMaterialDetail ordermaterialDetail = OrderMaterialDetail.fromRS(rs);
                list.add(ordermaterialDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public int getLatestId() {
        int latestId = 0;
        String sql = "SELECT MAX(ORDER_MATERIAL_ID) FROM ORDER_MATERIAL_DETAIL";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if (rs.next()) {
                latestId = rs.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return latestId;
    }
    
    @Override
    public List<OrderMaterialDetail> getAll(String where, String order) {
        ArrayList<OrderMaterialDetail> list = new ArrayList();
        String sql = "SELECT * FROM ORDER_MATERIAL_DETAIL where " + where + " ORDER BY" + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderMaterialDetail ordermaterialDetail = OrderMaterialDetail.fromRS(rs);
                list.add(ordermaterialDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<OrderMaterialDetail> getAll(String order) {
        ArrayList<OrderMaterialDetail> list = new ArrayList();
        String sql = "SELECT * FROM ORDER_MATERIAL_DETAIL ORDER BY" + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderMaterialDetail ordermaterialDetail = OrderMaterialDetail.fromRS(rs);
                list.add(ordermaterialDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
public OrderMaterialDetail save(OrderMaterialDetail obj) {
    String sql = "INSERT INTO ORDER_MATERIAL_DETAIL (ORDER_MATERIAL_DETAIL_UNIT,ORDER_MATERIAL_DETAIL_PRICE, ORDER_MATERIAL_ID, MATERIAL_ID) VALUES (?, ?, ?, ?)";
    Connection conn = ConnectDatabaseHelper.connectDatabase();
    try {
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, obj.getOrdermaterialdetailUnit());
        stmt.setFloat(2, obj.getOrdermaterialdetailPrice());
        stmt.setInt(3, obj.getOrdermaterialId());
        stmt.setInt(4, obj.getMaterialId());
        stmt.executeUpdate();
        int id = ConnectDatabaseHelper.getInsertedId(stmt);
        obj.setOrdermaterialdetailId(id);
    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
        return null;
    }
    return obj;
}


    @Override
    public OrderMaterialDetail update(OrderMaterialDetail obj) {
        String sql = "UPDATE ORDER_MATERIAL_DETAIL"
                + " SET ORDER_MATERIAL_DETAIL_UNIT=? ,ORDER_MATERIAL_DETAIL_PRICE=? , ORDER_MATERIAL_ID=?, MATERIAL_ID=?"
                + " WHERE ORDER_MATERIAL_DETAIL_ID = ?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getOrdermaterialdetailUnit());
            stmt.setFloat(2, obj.getOrdermaterialdetailPrice());
            stmt.setInt(3, obj.getOrdermaterialId());
            stmt.setInt(4, obj.getMaterialId());
            stmt.setInt(5, obj.getOrdermaterialdetailId());

//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(OrderMaterialDetail obj) {
        String sql = "DELETE FROM ORDER_MATERIAL_DETAIL WHERE ORDER_MATERIAL_DETAIL_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getOrdermaterialdetailId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
