/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.dao;

import com.mycompany.softdevtermprojectdecoffee.helper.ConnectDatabaseHelper;
import com.mycompany.softdevtermprojectdecoffee.model.Employee;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sabusi
 */
public class EmployeeDao implements Dao<Employee>{

    @Override
    public Employee get(int id) {
        Employee obj = null;
        String sql = "SELECT * FROM EMPLOYEE WHERE EMPLOYEE_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                obj = Employee.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return obj;
    }

    public Employee getByLogin(String name) {
        Employee obj = null;
        String sql = "SELECT * FROM EMPLOYEE WHERE EMPLOYEE_LOGIN=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                obj = Employee.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return obj;
    }

    public List<Employee> getAll() {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM EMPLOYEE";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee obj = Employee.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Employee> getAll(String where, String order) {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM EMPLOYEE where " + where + " ORDER BY" + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee obj = Employee.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Employee> getAll(String order) {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM EMPLOYEE  ORDER BY" + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee obj = Employee.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Employee save(Employee obj) {

        String sql = "INSERT INTO EMPLOYEE (EMPLOYEE_LOGIN, EMPLOYEE_NAME, EMPLOYEE_PASSWORD, EMPLOYEE_ROLE_ID, EMPLOYEE_GENDER, EMPLOYEE_TEL, EMPLOYEE_ADDRESS, EMPLOYEE_TYPE)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getLogin());
            stmt.setString(2, obj.getName());
            stmt.setString(3, obj.getPassword());
            stmt.setInt(4, obj.getRole());
            stmt.setString(5, obj.getGender());
            stmt.setString(6, obj.getTel());
            stmt.setString(7, obj.getAddress());
            stmt.setString(8, obj.getType());
            
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = ConnectDatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Employee update(Employee obj) {
        String sql = "UPDATE EMPLOYEE"
                + " SET EMPLOYEE_LOGIN = ?, EMPLOYEE_NAME = ?, EMPLOYEE_PASSWORD = ?, EMPLOYEE_ROLE_ID = ?, EMPLOYEE_GENDER = ?, EMPLOYEE_TEL = ?, EMPLOYEE_ADDRESS = ?, EMPLOYEE_TYPE = ?"
                + " WHERE EMPLOYEE_ID = ?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getLogin());
            stmt.setString(2, obj.getName());
            stmt.setString(3, obj.getPassword());
            stmt.setInt(4, obj.getRole());
            stmt.setString(5, obj.getGender());
            stmt.setString(6, obj.getTel());
            stmt.setString(7, obj.getAddress());
            stmt.setString(8, obj.getType());
            stmt.setInt(9, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Employee obj) {
        String sql = "DELETE FROM EMPLOYEE WHERE EMPLOYEE_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
    
}
