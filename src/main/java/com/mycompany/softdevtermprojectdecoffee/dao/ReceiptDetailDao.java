/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.dao;

import com.mycompany.softdevtermprojectdecoffee.helper.ConnectDatabaseHelper;
import com.mycompany.softdevtermprojectdecoffee.model.ReceiptDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ReceiptDetailDao implements Dao<ReceiptDetail> {

    @Override
    public ReceiptDetail get(int id) {
        ReceiptDetail recieptDetail = null;
        String sql = "SELECT * FROM RECEIPT_DETAIL WHERE RECEIPT_DETAIL_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                recieptDetail = ReceiptDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return recieptDetail;
    }

    public List<ReceiptDetail> getAll() {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT_DETAIL";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptDetail recieptDetail = ReceiptDetail.fromRS(rs);
                list.add(recieptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<ReceiptDetail> getAll(String where, String order) {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT_DETAIL where " + where + " ORDER BY" + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptDetail recieptDetail = ReceiptDetail.fromRS(rs);
                list.add(recieptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReceiptDetail> getAll(String order) {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT_DETAIL  ORDER BY" + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptDetail recieptDetail = ReceiptDetail.fromRS(rs);
                list.add(recieptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public ReceiptDetail save(ReceiptDetail obj) {

        String sql = "INSERT INTO RECEIPT_DETAIL (PRODUCT_ID, RECEIPT_DETAIL_NAME, RECEIPT_DETAIL_PRODUCT_PRICE, RECEIPT_DETAIL_QUANTITY, RECEIPT_DETAIL_TOTALPRICE, RECEIPT_ID, PROMOTION_DETAIL_ID)"
                + "VALUES(?, ?, ?, ?, ?,?,?)";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProductId());
            stmt.setString(2, obj.getProductName());
            stmt.setFloat(3, obj.getProductPrice());
            stmt.setInt(4, obj.getQty());
            stmt.setFloat(5, obj.getTotalPrice());
            stmt.setInt(6, obj.getReceiptId());
            stmt.setInt(7, obj.getPromotionId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = ConnectDatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public ReceiptDetail update(ReceiptDetail obj) {
        String sql = "UPDATE RECEIPT_DETAIL"
                + " SET PRODUCT_ID = ?, RECEIPT_DETAIL_NAME = ?, RECEIPT_DETAIL_PRODUCT_PRICE = ?, RECEIPT_DETAIL_QUANTITY = ?, RECEIPT_DETAIL_TOTALPRICE = ?, RECEIPT_ID = ?, PROMOTION_DETAIL_ID = ?"
                + " WHERE RECEIPT_DETAIL_ID = ?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getProductId());
            stmt.setString(2, obj.getProductName());
            stmt.setFloat(3, obj.getProductPrice());
            stmt.setInt(4, obj.getQty());
            stmt.setFloat(5, obj.getTotalPrice());
            stmt.setInt(6, obj.getReceiptId());
            stmt.setInt(7, obj.getPromotionId());
            stmt.setInt(8, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(ReceiptDetail obj) {
        String sql = "DELETE FROM RECEIPT_DETAIL WHERE RECEIPT_DETAIL_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
    
//    public List<ReceiptDetail> getBestSeller() {
//        ArrayList<ReceiptDetail> list = new ArrayList();
//        String sql = "SELECT PRODUCT_ID, SUM(RECEIPT_DETAIL_QUANTITY) AS TOTAL_QUANTITY_SOLD" 
//                    + "FROM RECEIPT_DETAIL"
//                    + "GROUP BY PRODUCT_ID"
//                    + "ORDER BY TOTAL_QUANTITY_SOLD DESC"
//                    + "LIMIT 10;";
//        Connection conn = ConnectDatabaseHelper.connectDatabase();
//        try {
//            Statement stmt = conn.createStatement();
//            ResultSet rs = stmt.executeQuery(sql);
//
//            while (rs.next()) {
//                ReceiptDetail recieptDetail = ReceiptDetail.fromRS(rs);
//                list.add(recieptDetail);
//            }
//
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
//        return list;
//    }
    
    public List<ReceiptDetail> getBestSeller() {
    ArrayList<ReceiptDetail> list = new ArrayList();
    String sql = "SELECT PRODUCT_ID, SUM(RECEIPT_DETAIL_QUANTITY) AS TOTAL_QUANTITY_SOLD " 
                + "FROM RECEIPT_DETAIL "
                + "GROUP BY PRODUCT_ID "
                + "ORDER BY TOTAL_QUANTITY_SOLD DESC "
                + "LIMIT 10;";
    Connection conn = ConnectDatabaseHelper.connectDatabase();
    try {
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        while (rs.next()) {
            int productId = rs.getInt("PRODUCT_ID");
            int totalQuantitySold = rs.getInt("TOTAL_QUANTITY_SOLD");
            
            // Create a ReceiptDetail object with the retrieved data
            ReceiptDetail receiptDetail = new ReceiptDetail();
            receiptDetail.setProductId(productId);
            receiptDetail.setQty(totalQuantitySold);
            
            list.add(receiptDetail);
        }

    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
    }
    return list;
}
}
