
package com.mycompany.softdevtermprojectdecoffee.dao;

import com.mycompany.softdevtermprojectdecoffee.helper.ConnectDatabaseHelper;
import com.mycompany.softdevtermprojectdecoffee.model.Bills;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class BillsDao implements Dao<Bills>{

    @Override
    public Bills get(int id) {
        Bills bills = null;
        String sql = "SELECT * FROM BILLS WHERE BILLS_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                bills = bills.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return bills;
    }

    @Override
    public List<Bills> getAll() {
        ArrayList<Bills> list = new ArrayList();
        String sql = "SELECT * FROM BILLS";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bills bills = Bills.fromRS(rs);
                list.add(bills);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Bills> getAll(String where, String order) {
        ArrayList<Bills> list = new ArrayList();
        String sql = "SELECT * FROM BILLS where " + where + " ORDER BY " + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bills bills = Bills.fromRS(rs);
                list.add(bills);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Bills> getAll(String order) {
        ArrayList<Bills> list = new ArrayList();
        String sql = "SELECT * FROM BILLS ORDER BY " + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bills bills = Bills.fromRS(rs);
                list.add(bills);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
public Bills save(Bills obj) {
    String sql = "INSERT INTO BILLS (BILLS_WATER,BILLS_ELECTRIC,BILLS_PAYDATE,BILLS_WTB,BILLS_ETB,BILLS_VAT,BILLS_USEDWATER,BILLS_USEDELECTRIC) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    Connection conn = ConnectDatabaseHelper.connectDatabase();
    try {
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setFloat(1, obj.getWater());
        stmt.setFloat(2, obj.getElectric());
        stmt.setString(3, obj.getPaydate());
        stmt.setFloat(4, obj.getWtb());
        stmt.setFloat(5, obj.getEtb());
        stmt.setFloat(6, obj.getVat());
        stmt.setFloat(7, obj.getUsedwater());
        stmt.setFloat(8, obj.getUsedelectric());
        stmt.executeUpdate();
        int id = ConnectDatabaseHelper.getInsertedId(stmt);
        obj.setId(id);
    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
        return null;
    }
    return obj;
}


    @Override
    public Bills update(Bills obj) {
        String sql = "UPDATE BILLS"
                + " SET BILLS_WATER=? ,BILLS_ELECTRIC=? ,BILLS_PAYDATE=?,BILLS_WTB=?,BILLS_ETB=?,BILLS_VAT=?,BILLS_USEDWATER=?,BILLS_USEDELECTRIC=?"
                + " WHERE BILLS_ID = ?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getWater());
            stmt.setFloat(2, obj.getElectric());
            stmt.setString(3, obj.getPaydate());
            stmt.setFloat(4, obj.getWtb());
            stmt.setFloat(5, obj.getEtb());
            stmt.setFloat(6, obj.getVat());
            stmt.setFloat(7, obj.getUsedwater());
            stmt.setFloat(8, obj.getUsedelectric());
            stmt.setInt(9, obj.getId());

//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Bills obj) {
        String sql = "DELETE FROM BILLS WHERE BILLS_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
