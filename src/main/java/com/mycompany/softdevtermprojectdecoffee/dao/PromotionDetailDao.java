
package com.mycompany.softdevtermprojectdecoffee.dao;

import com.mycompany.softdevtermprojectdecoffee.helper.ConnectDatabaseHelper;
import com.mycompany.softdevtermprojectdecoffee.model.PromotionDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class PromotionDetailDao implements Dao<PromotionDetail>{

    @Override
    public PromotionDetail get(int id) {
        PromotionDetail promotionDetail = null;
        String sql = "SELECT * FROM PROMOTION_DETAIL WHERE PROMOTION_DETAIL_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                promotionDetail = promotionDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promotionDetail;
    }
    
        public PromotionDetail getByCondition(String condition) {
        PromotionDetail promotionDetail = null;
        String sql = "SELECT * FROM PROMOTION_DETAIL WHERE PROMOTION_DETAIL_CONDITION=? AND PROMOTION_DETAIL_STATUS = 'On'";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, condition);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                promotionDetail = promotionDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promotionDetail;
    }

    public List<PromotionDetail> getAll() {
        ArrayList<PromotionDetail> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTION_DETAIL";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PromotionDetail promotionDetail = PromotionDetail.fromRS(rs);
                list.add(promotionDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<PromotionDetail> getAll(String where, String order) {
        ArrayList<PromotionDetail> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTION_DETAIL where " + where + " ORDER BY" + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PromotionDetail promotionDetail = PromotionDetail.fromRS(rs);
                list.add(promotionDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<PromotionDetail> getAll(String order) {
        ArrayList<PromotionDetail> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTION_DETAIL ORDER BY" + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PromotionDetail promotionDetail = PromotionDetail.fromRS(rs);
                list.add(promotionDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
public PromotionDetail save(PromotionDetail obj) {
    String sql = "INSERT INTO PROMOTION_DETAIL (PROMOTION_DETAIL_DISCOUNT, PROMOTION_DETAIL_STATUS, PROMOTION_DETAIL_DESCRIPT, PROMOTION_DETAIL_CONDITION)" 
            + "VALUES (?, ?, ?, ?)";
    Connection conn = ConnectDatabaseHelper.connectDatabase();
    try {
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setFloat(1, obj.getPromotionDetailDiscount());
        stmt.setString(2, obj.getPromotionDetailStatus());
        stmt.setString(3, obj.getPromotionDetailDescript());
        stmt.setString(4, obj.getPromotionDetailCondition());
        stmt.executeUpdate();
        int id = ConnectDatabaseHelper.getInsertedId(stmt);
        obj.setPromotionDetailDetailId(id);
    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
        return null;
    }
    return obj;
}


    @Override
    public PromotionDetail update(PromotionDetail obj) {
        String sql = "UPDATE PROMOTION_DETAIL"
                + " SET PROMOTION_DETAIL_DISCOUNT=? ,PROMOTION_DETAIL_STATUS=? , PROMOTION_DETAIL_DESCRIPT=?, PROMOTION_DETAIL_CONDITION=?"
                + " WHERE PROMOTION_DETAIL_ID = ?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getPromotionDetailDiscount());
            stmt.setString(2, obj.getPromotionDetailStatus());
            stmt.setString(3, obj.getPromotionDetailDescript());
            stmt.setString(4, obj.getPromotionDetailCondition());
            stmt.setInt(5, obj.getPromotionDetailDetailId());

//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(PromotionDetail obj) {
        String sql = "DELETE FROM PROMOTION_DETAIL WHERE PROMOTION_DETAIL_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getPromotionDetailDetailId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
