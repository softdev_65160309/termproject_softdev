/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.dao;

import com.mycompany.softdevtermprojectdecoffee.helper.ConnectDatabaseHelper;
import com.mycompany.softdevtermprojectdecoffee.model.ReportWorkingTime;
import com.mycompany.softdevtermprojectdecoffee.model.WorkingTime;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author HP
 */
public class ReportWorkingTimeDao implements Dao<ReportWorkingTimeDao>{

    @Override
    public ReportWorkingTimeDao get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<ReportWorkingTimeDao> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ReportWorkingTimeDao save(ReportWorkingTimeDao obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ReportWorkingTimeDao update(ReportWorkingTimeDao obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(ReportWorkingTimeDao obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<ReportWorkingTimeDao> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    public ReportWorkingTime get(int id, String selectedMonth) {
        ArrayList<ReportWorkingTime> list = new ArrayList();
        ReportWorkingTime workingtime = null;
        String sql = "SELECT (strftime('%s', WORKINGTIME_CHECKOUT) - strftime('%s', WORKINGTIME_CHECKIN)) / 3600 AS time_difference, "
                     + "strftime('%Y-%m', WORKINGTIME_CHECKIN) AS month "
                     + "FROM WORKINGTIME "
                     + "WHERE EMPLOYEE_ID = ? AND strftime('%Y-%m', WORKINGTIME_CHECKIN) = ? "
                     + "GROUP BY EMPLOYEE_ID, month;";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.setString(2, selectedMonth);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
               workingtime = ReportWorkingTime.fromRS(rs);
//                list.add(workingtime);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return workingtime;
    }
}
