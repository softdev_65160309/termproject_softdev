/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.dao;

import com.mycompany.softdevtermprojectdecoffee.helper.ConnectDatabaseHelper;
import com.mycompany.softdevtermprojectdecoffee.model.Category;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Raniny
 */
public class CategoryDao implements Dao<Category>{

    @Override
    public Category get(int id) {
        Category category = null;
        String sql = "SELECT * FROM CATEGORY WHERE CATEGORY_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                category = Category.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return category;
    }

    public List<Category> getAll() {
        ArrayList<Category> list = new ArrayList();
        String sql = "SELECT * FROM CATEGORY";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Category category = Category.fromRS(rs);
                list.add(category);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Category> getAll(String where, String order) {
        ArrayList<Category> list = new ArrayList();
        String sql = "SELECT * FROM CATEGORY where " + where + " ORDER BY" + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Category category = Category.fromRS(rs);
                list.add(category);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Category> getAll(String order) {
        ArrayList<Category> list = new ArrayList();
        String sql = "SELECT * FROM CATEGORY  ORDER BY" + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Category category = Category.fromRS(rs);
                list.add(category);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Category save(Category obj) {
        String sql = "INSERT INTO category (CATEGORY_NAME)"
                + "VALUES(?)";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getCategoryName());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = ConnectDatabaseHelper.getInsertedId(stmt);
            obj.setCategoryId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Category update(Category obj) {
        String sql = "UPDATE CATEGORY"
                + " SET CATEGORY_NAME=?"
                + " WHERE CATEGORY_ID = ?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getCategoryName());
            stmt.setInt(2, obj.getCategoryId());

//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Category obj) {
        String sql = "DELETE FROM CATEGORY WHERE CATEGORY_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCategoryId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
