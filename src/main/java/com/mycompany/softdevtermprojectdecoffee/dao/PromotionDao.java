
package com.mycompany.softdevtermprojectdecoffee.dao;

import com.mycompany.softdevtermprojectdecoffee.helper.ConnectDatabaseHelper;
import com.mycompany.softdevtermprojectdecoffee.model.Promotion;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class PromotionDao implements Dao<Promotion>{

    @Override
    public Promotion get(int id) {
        Promotion promotion = null;
        String sql = "SELECT * FROM PROMOTION WHERE PROMOTION_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                promotion = promotion.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promotion;
    }

    public List<Promotion> getAll() {
        ArrayList<Promotion> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTION";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                list.add(promotion);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Promotion> getAll(String where, String order) {
        ArrayList<Promotion> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTION where " + where + " ORDER BY " + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                list.add(promotion);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Promotion> getAll(String order) {
        ArrayList<Promotion> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTION ORDER BY " + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                list.add(promotion);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
public Promotion save(Promotion obj) {
    String sql = "INSERT INTO PROMOTION (PROMOTION_NAME,PROMOTION_STARTDATE,PROMOTION_ENDDATE,PROMOTION_STATUS, PROMOTION_DESCRIPTION) VALUES (?, ?, ?, ?, ?)";
    Connection conn = ConnectDatabaseHelper.connectDatabase();
    try {
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, obj.getPromotionName());
        stmt.setString(2, obj.getPromotionstartDate());
        stmt.setString(3, obj.getPromotionendDate());
        stmt.setString(4, obj.getPromotionStatus());
        stmt.setString(5, obj.getPromotionDescription());
        stmt.executeUpdate();
        int id = ConnectDatabaseHelper.getInsertedId(stmt);
        obj.setPromotionId(id);
    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
        return null;
    }
    return obj;
}


    @Override
    public Promotion update(Promotion obj) {
        String sql = "UPDATE PROMOTION"
                + " SET PROMOTION_NAME=? ,PROMOTION_STATUS=? ,PROMOTION_STARTDATE=?,PROMOTION_ENDDATE=?, PROMOTION_DESCRIPTION=?"
                + " WHERE PROMOTION_ID = ?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
             stmt.setString(1, obj.getPromotionName());
            stmt.setString(2, obj.getPromotionStatus());
            stmt.setString(3, obj.getPromotionstartDate());
            stmt.setString(4, obj.getPromotionendDate());
            stmt.setString(5, obj.getPromotionDescription());
            stmt.setInt(6, obj.getPromotionId());

//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Promotion obj) {
        String sql = "DELETE FROM PROMOTION WHERE PROMOTION_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getPromotionId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
