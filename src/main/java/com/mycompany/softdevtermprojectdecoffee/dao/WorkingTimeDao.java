/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.dao;

import com.mycompany.softdevtermprojectdecoffee.helper.ConnectDatabaseHelper;
import com.mycompany.softdevtermprojectdecoffee.model.WorkingTime;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author supha
 */
public class WorkingTimeDao implements Dao<WorkingTime> {
        @Override
    public WorkingTime get(int id) {
        WorkingTime workingtime = null;
        String sql = "SELECT * FROM WORKINGTIME WHERE WORKINGTIME_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                workingtime = WorkingTime.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return workingtime;
    }

    public List<WorkingTime> getAll() {
        ArrayList<WorkingTime> list = new ArrayList();
        String sql = "SELECT * FROM WORKINGTIME";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                WorkingTime workingtime = WorkingTime.fromRS(rs);
                list.add(workingtime);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<WorkingTime> getAll(String where, String order) {
        ArrayList<WorkingTime> list = new ArrayList();
        String sql = "SELECT * FROM WORKINGTIME where " + where + " ORDER BY" + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                WorkingTime workingtime = WorkingTime.fromRS(rs);
                list.add(workingtime);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<WorkingTime> getAll(String order) {
        ArrayList<WorkingTime> list = new ArrayList();
        String sql = "SELECT * FROM WORKINGTIME ORDER BY" + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                WorkingTime workingtime = WorkingTime.fromRS(rs);
                list.add(workingtime);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public WorkingTime save(WorkingTime obj) {

        String sql = "INSERT INTO WORKINGTIME (SALARY_ID, EMPLOYEE_ID, WORKINGTIME_TOTALHOUR)"
                + "VALUES(?, ?, ?)";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSalaryId());
            stmt.setInt(2, obj.getEmployeeId());
            stmt.setFloat(3, obj.getTotalHour());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = ConnectDatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public WorkingTime update(WorkingTime obj) {
        String sql = "UPDATE WORKINGTIME"
                + " SET SALARY_ID = ?, EMPLOYEE_ID = ?, WORKINGTIME_TOTALHOUR = ?"
                + " WHERE WORKINGTIME_ID = ?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSalaryId());
            stmt.setInt(2, obj.getEmployeeId());
            stmt.setFloat(3, obj.getTotalHour());
            stmt.setInt(4, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) { 
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    @Override
    public int delete(WorkingTime obj) {
        String sql = "DELETE FROM WORKINGTIME WHERE WORKINGTIME_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
    public List<WorkingTime> getEmp(int id) {
        ArrayList<WorkingTime>  workingtime = new ArrayList<>();
        String sql = "SELECT * FROM WORKINGTIME WHERE EMPLOYEE_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                WorkingTime time = WorkingTime.fromRS(rs) ;
                workingtime.add(time);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return  workingtime;
    }
}
