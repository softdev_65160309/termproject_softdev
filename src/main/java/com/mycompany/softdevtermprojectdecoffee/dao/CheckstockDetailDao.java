/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.dao;

import com.mycompany.softdevtermprojectdecoffee.helper.ConnectDatabaseHelper;
import com.mycompany.softdevtermprojectdecoffee.model.CheckstockDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class CheckstockDetailDao implements Dao<CheckstockDetail>{

    @Override
    public CheckstockDetail get(int id) {
        CheckstockDetail recieptDetail = null;
            String sql = "SELECT * FROM CHECKSTOCK_DETAIL WHERE CHECKSTOCK_DETAIL_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                recieptDetail = CheckstockDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return recieptDetail;
    }

    public List<CheckstockDetail> getAll() {
        ArrayList<CheckstockDetail> list = new ArrayList();
        String sql = "SELECT * FROM CHECKSTOCK_DETAIL";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckstockDetail recieptDetail = CheckstockDetail.fromRS(rs);
                list.add(recieptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CheckstockDetail> getAll(String where, String order) {
        ArrayList<CheckstockDetail> list = new ArrayList();
        String sql = "SELECT * FROM CHECKSTOCK_DETAIL where " + where + " ORDER BY" + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckstockDetail recieptDetail = CheckstockDetail.fromRS(rs);
                list.add(recieptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<CheckstockDetail> getLatest(String order) {
        ArrayList<CheckstockDetail> list = new ArrayList();
        String sql = "SELECT * FROM CHECKSTOCK_DETAIL "
                + "WHERE CHECKSTOCK_ID = (SELECT MAX(CHECKSTOCK_ID) FROM CHECKSTOCK) "
                + "ORDER BY " + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckstockDetail recieptDetail = CheckstockDetail.fromRS(rs);
                list.add(recieptDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckstockDetail> getAll(String order) {
        ArrayList<CheckstockDetail> list = new ArrayList();
        String sql = "SELECT * FROM CHECKSTOCK_DETAIL  ORDER BY" + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckstockDetail recieptDetail = CheckstockDetail.fromRS(rs);
                list.add(recieptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckstockDetail save(CheckstockDetail obj) {

        String sql = "INSERT INTO CHECKSTOCK_DETAIL (CHECKSTOCK_DETAIL_UNIT, CHECKSTOCK_ID, MATERIAL_ID, CHECKSTOCK_DETAIL_PRICE)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getUnit());
            stmt.setInt(2, obj.getCheckstockId());
            stmt.setInt(3, obj.getMaterialId());
            stmt.setFloat(4, obj.getPrice());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = ConnectDatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckstockDetail update(CheckstockDetail obj) {
        String sql = "UPDATE CHECKSTOCK_DETAIL"
                + " SET CHECKSTOCK_DETAIL_UNIT = ?, CHECKSTOCK_ID = ?, MATERIAL_ID = ?, CHECKSTOCK_DETAIL_PRICE = ?"
                + " WHERE CHECKSTOCK_DETAIL_ID = ?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setInt(2, obj.getUnit());
            stmt.setInt(3, obj.getCheckstockId());
            stmt.setInt(4, obj.getMaterialId());
            stmt.setFloat(5, obj.getPrice());
            stmt.setInt(6, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckstockDetail obj) {
        String sql = "DELETE FROM CHECKSTOCK_DETAIL WHERE CHECKSTOCK_DETAIL_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
    
    public int getLatestId() {
        int latestId = 0; 
        String sql = "SELECT MAX(CHECKSTOCK_ID) FROM CHECKSTOCK_DETAIL";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if (rs.next()) {
                latestId = rs.getInt(1); 
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return latestId;
    }

    public int getTotalPrice() {
        int totalPrice = 0; 
        int latestCheckStockId = getLatestId(); 
        String sql = "SELECT SUM(CHECKSTOCK_DETAIL_UNIT * CHECKSTOCK_DETAIL_PRICE) "
                + "FROM CHECKSTOCK_DETAIL "
                + "WHERE CHECKSTOCK_ID = ?";

        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, latestCheckStockId);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                totalPrice = rs.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return totalPrice;
    }
}
