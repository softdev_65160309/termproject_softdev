/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.dao;

import com.mycompany.softdevtermprojectdecoffee.helper.ConnectDatabaseHelper;
import com.mycompany.softdevtermprojectdecoffee.model.OrderMaterial;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderMaterialDao implements Dao<OrderMaterial> {

    @Override
    public OrderMaterial get(int id) {
        OrderMaterial ordermaterial = null;
        String sql = "SELECT * FROM ORDER_MATERIAL WHERE ORDER_MATERIAL_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ordermaterial = OrderMaterial.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return ordermaterial;
    }

    public List<OrderMaterial> getAll() {
        ArrayList<OrderMaterial> list = new ArrayList();
        String sql = "SELECT * FROM ORDER_MATERIAL";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderMaterial ordermaterial = OrderMaterial.fromRS(rs);
                list.add(ordermaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<OrderMaterial> getAll(String where, String order) {
        ArrayList<OrderMaterial> list = new ArrayList();
        String sql = "SELECT * FROM ORDER_MATERIAL where " + where + " ORDER BY" + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderMaterial ordermaterial = OrderMaterial.fromRS(rs);
                list.add(ordermaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<OrderMaterial> getAll(String order) {
        ArrayList<OrderMaterial> list = new ArrayList();
        String sql = "SELECT * FROM ORDER_MATERIAL  ORDER BY" + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderMaterial ordermaterial = OrderMaterial.fromRS(rs);
                list.add(ordermaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public OrderMaterial save(OrderMaterial obj) {
        String sql = "INSERT INTO ORDER_MATERIAL (ORDER_MATERIAL_TOTAL_PRICE, EMPLOYEE_ID)"
                + "VALUES(?, ?)";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotalprice());
            stmt.setInt(2, obj.getEmid());
            stmt.executeUpdate();
            int id = ConnectDatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public OrderMaterial update(OrderMaterial obj) {
        String sql = "UPDATE ORDER_MATERIAL"
                + " SET ORDER_MATERIAL_TOTAL_PRICE=?, EMPLOYEE_ID=?"
                + " WHERE ORDER_MATERIAL_ID = ?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotalprice());
            stmt.setInt(2, obj.getEmid());
            stmt.setInt(3, obj.getId());

            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(OrderMaterial obj) {
        String sql = "DELETE FROM ORDER_MATERIAL WHERE ORDER_MATERIAL_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
