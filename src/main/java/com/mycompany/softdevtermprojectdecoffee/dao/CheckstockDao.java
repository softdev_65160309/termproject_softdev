/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.dao;

import com.mycompany.softdevtermprojectdecoffee.helper.ConnectDatabaseHelper;
import com.mycompany.softdevtermprojectdecoffee.model.Checkstock;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class CheckstockDao implements Dao<Checkstock>{
    @Override
    public Checkstock get(int id) {
        Checkstock checkstock = null;
        String sql = "SELECT * FROM CHECKSTOCK WHERE CHECKSTOCK_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkstock = Checkstock.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkstock;
    }
    
    public List<Checkstock> getAll() {
        ArrayList<Checkstock> list = new ArrayList();
        String sql = "SELECT * FROM CHECKSTOCK";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Checkstock checkstock = Checkstock.fromRS(rs);
                list.add(checkstock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Checkstock> getAll(String where, String order) {
        ArrayList<Checkstock> list = new ArrayList();
        String sql = "SELECT * FROM CHECKSTOCK where " + where + " ORDER BY" + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Checkstock checkstock = Checkstock.fromRS(rs);
                list.add(checkstock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Checkstock> getAll(String order) {
        ArrayList<Checkstock> list = new ArrayList();
        String sql = "SELECT * FROM CHECKSTOCK ORDER BY" + order;
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Checkstock checkstock = Checkstock.fromRS(rs);
                list.add(checkstock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Checkstock save(Checkstock obj) {
        String sql = "INSERT INTO CHECKSTOCK (CHECKSTOCK_TOTAL_PRICE, EMPLOYEE_ID)"
                + "VALUES(?, ?)";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getTotalUnit());
            stmt.setInt(2, obj.getEmpId());
            

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = ConnectDatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Checkstock update(Checkstock obj) {
        String sql = "UPDATE CHECKSTOCK"
                + " SET CHECKSTOCK_TOTAL_PRICE = ?,EMPLOYEE_ID = ?"
                + " WHERE CHECKSTOCK_ID = ?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getTotalUnit());
            stmt.setInt(2, obj.getEmpId());
            stmt.setInt(3, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    @Override
    public int delete(Checkstock obj) {
        String sql = "DELETE FROM CHECKSTOCK WHERE CHECKSTOCK_ID=?";
        Connection conn = ConnectDatabaseHelper.connectDatabase();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    } 
}
