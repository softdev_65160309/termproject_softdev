/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.Testing;

import com.mycompany.softdevtermprojectdecoffee.model.Product;
import com.mycompany.softdevtermprojectdecoffee.service.ProductService;

/**
 *
 * @author Sabusi
 */
public class ProductServiceTest {
    public static void main(String[] args) {
        ProductService productService = new ProductService();
        for(Product product : productService.getProducts()){
            System.out.println(product);
        }
    }
}
