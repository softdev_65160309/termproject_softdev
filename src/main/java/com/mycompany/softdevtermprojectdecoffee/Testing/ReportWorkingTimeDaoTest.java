/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.Testing;

import com.mycompany.softdevtermprojectdecoffee.dao.ReportWorkingTimeDao;
import com.mycompany.softdevtermprojectdecoffee.model.ReportWorkingTime;
import com.mycompany.softdevtermprojectdecoffee.service.ReportWorkingTimeService;
import java.time.LocalDate;

/**
 *
 * @author HP
 */
public class ReportWorkingTimeDaoTest {
    public static void main(String[] args) {
        ReportWorkingTimeDao rd = new ReportWorkingTimeDao();
        LocalDate today = LocalDate.now();

        int year = today.getYear();
        int month = today.getMonthValue();

        String yearMonth = String.format("%d-%02d", year, month);

        System.out.println(yearMonth);
        System.out.println(rd.get(1, yearMonth));
        ReportWorkingTimeService rs = new ReportWorkingTimeService();
//        ReportWorkingTime report = rs.getTotalHourByMonthAndEmp(2, yearMonth);
//        System.out.println(report.getTime_difference());
        
//        ReportWorkingTime re = rs.getTotalHourByMonthAndEmp(1, yearMonth));
//        System.out.println(re.getTime_difference());
        
    }
}
