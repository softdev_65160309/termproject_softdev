/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.Testing;

import com.mycompany.softdevtermprojectdecoffee.model.Employee;
import com.mycompany.softdevtermprojectdecoffee.service.EmployeeService;

/**
 *
 * @author Sabusi
 */
public class EmployeeServiceTest {
    public static void main(String[] args) {
                EmployeeService employeeSevice = new EmployeeService();
        Employee employee = employeeSevice.login("Kululu_001", "cW8DM1cMOw");
        if(employee!=null) {
            System.out.println("Welcome user : " + employee.getName());
        } else {
            System.out.println("Error");
        }
    }
}
