/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.Testing;
import com.mycompany.softdevtermprojectdecoffee.model.Receipt;
import com.mycompany.softdevtermprojectdecoffee.service.ReceiptService;

/**
 *
 * @author Sabusi
 */
public class ReceiptServiceTest {
    public static void main(String[] args) {
        ReceiptService receiptService = new ReceiptService();
        for(Receipt receipt : receiptService.getReceipt()) {
            System.out.println(receipt);
        }
    }
}
