/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.Testing;


import com.mycompany.softdevtermprojectdecoffee.dao.ProductDao;
import com.mycompany.softdevtermprojectdecoffee.dao.ReceiptDao;
import com.mycompany.softdevtermprojectdecoffee.dao.ReceiptDetailDao;
import com.mycompany.softdevtermprojectdecoffee.model.Product;
import com.mycompany.softdevtermprojectdecoffee.model.Receipt;
import com.mycompany.softdevtermprojectdecoffee.model.ReceiptDetail;
import java.util.List;

/**
 *
 * @author Sabusi
 */
public class ReceiptDetailDaoTest {
    public static void main(String[] args) {
        ReceiptDetailDao rdd = new ReceiptDetailDao();
        for(ReceiptDetail rd :rdd.getAll()){
            System.out.println(rd);
        }
        
        ReceiptDao rd = new ReceiptDao();
        ProductDao pd = new ProductDao();
        List<Product> products = pd.getAll();
        Product product0 = products.get(1);
        Receipt reciept = rd.get(1);
        ReceiptDetail newRecieptDetail = new ReceiptDetail(product0.getId(), product0.getName(), product0.getPrice(), 2, product0.getPrice()*500, 1,1);
        rdd.save(newRecieptDetail);

    }
}
