/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.Testing;

import com.mycompany.softdevtermprojectdecoffee.dao.ProductDao;
import com.mycompany.softdevtermprojectdecoffee.helper.ConnectDatabaseHelper;
import com.mycompany.softdevtermprojectdecoffee.model.Product;

/**
 *
 * @author Sabusi
 */
public class ProductDaoTest {
    public static void main(String[] args) {
        ProductDao productDao = new ProductDao();
        for(Product p: productDao.getAll()) {
            System.out.println("Product : "+ p);
        }
        
        ConnectDatabaseHelper.close();
    }
}
