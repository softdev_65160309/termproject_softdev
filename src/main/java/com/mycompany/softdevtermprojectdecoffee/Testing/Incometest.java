/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.Testing;

import com.mycompany.softdevtermprojectdecoffee.model.Receipt;
import com.mycompany.softdevtermprojectdecoffee.service.ReceiptService;
import java.util.List;

/**
 *
 * @author boat5
 */
public class Incometest {
    private final ReceiptService receiptService = new ReceiptService();
    List<Receipt> receipt = receiptService.getReceipt();
    
    
    private float calIncome(List<Receipt> receipt) {
        float Income = 0;
        for(int i = 0; i < receipt.size(); i++){
            Receipt income = receipt.get(i);
            System.out.println(income.getTotal());
            Income = Income + income.getTotal();
        }
        return Income;
    }
    
    public static void main(String[] args) {
        Incometest incometest = new Incometest();
        System.out.println("Income : "+incometest.calIncome(incometest.receipt));

    }
}
