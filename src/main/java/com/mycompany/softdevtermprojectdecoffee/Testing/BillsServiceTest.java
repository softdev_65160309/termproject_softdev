/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.Testing;

import com.mycompany.softdevtermprojectdecoffee.model.Bills;
import com.mycompany.softdevtermprojectdecoffee.service.BillsService;


public class BillsServiceTest {

    public static void main(String[] args) {
        BillsService billsService = new BillsService();
        for (Bills bill : billsService.getBills()) {
            System.out.println(bill);
        }
    }
}
