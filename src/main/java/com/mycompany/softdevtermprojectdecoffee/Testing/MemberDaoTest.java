/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.Testing;

import com.mycompany.softdevtermprojectdecoffee.dao.MemberDao;
import com.mycompany.softdevtermprojectdecoffee.helper.ConnectDatabaseHelper;
import com.mycompany.softdevtermprojectdecoffee.model.Member;

/**
 *
 * @author Sabusi
 */
public class MemberDaoTest {
    public static void main(String[] args) {
       MemberDao memberDao = new MemberDao();
        for(Member m: memberDao.getAll()) {
            System.out.println("Member : "+ m);
        }
        
        ConnectDatabaseHelper.close();
    }
}
