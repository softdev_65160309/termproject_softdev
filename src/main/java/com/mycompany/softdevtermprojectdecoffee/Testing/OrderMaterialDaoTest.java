package com.mycompany.softdevtermprojectdecoffee.Testing;

import com.mycompany.softdevtermprojectdecoffee.dao.OrderMaterialDao;
import com.mycompany.softdevtermprojectdecoffee.helper.ConnectDatabaseHelper;
import com.mycompany.softdevtermprojectdecoffee.model.OrderMaterial;

public class OrderMaterialDaoTest {
    public static void main(String[] args) {
        OrderMaterialDao ordermaterialDao = new OrderMaterialDao();
        for(OrderMaterial p: ordermaterialDao.getAll()) {
            System.out.println("Ordermaterial : "+ p);
        }
        
        ConnectDatabaseHelper.close();
    }
}