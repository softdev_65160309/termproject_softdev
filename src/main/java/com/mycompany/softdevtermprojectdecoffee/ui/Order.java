/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.ui;

import com.mycompany.softdevtermprojectdecoffee.model.OrderMaterial;
import com.mycompany.softdevtermprojectdecoffee.model.OrderMaterialDetail;
import com.mycompany.softdevtermprojectdecoffee.service.OrderMaterialService;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import java.time.LocalDateTime;
import javax.swing.JOptionPane;

/**
 *
 * @author Fiawfaw
 */
public class Order extends javax.swing.JFrame {

    private List<OrderMaterial> list;
    private OrderMaterialService ordermaterialService;
    private OrderMaterial editedOrderMaterial;
    private List<OrderMaterialDetail> listd;
    private OrderMaterialService ordermaterialdService;
    private OrderMaterialDetail editedOrderMaterialDetail;
    private float total;
    private OrderMaterialDetail ordermaterialdetail;

    /**
     * Creates new form Order
     */
    public Order() {
        initComponents();
        txtomid.setEnabled(false);
        txtTime.setEnabled(false);
        txtomdid.setEnabled(false);
        txtomtp.setEnabled(false);
        editedOrderMaterial = new OrderMaterial();
        editedOrderMaterialDetail = new OrderMaterialDetail();

        ordermaterialService = new OrderMaterialService();
        list = ordermaterialService.getOrderMaterial();
        tblOrdermaterial.setRowHeight(100);
        tblOrdermaterial.setModel(new AbstractTableModel() {
            String[] columnNames = {"Order material ID", "Date", "Totoprice", "Employee ID",};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                OrderMaterial ordermaterial = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return ordermaterial.getId();
                    case 1:
                        return ordermaterial.getCreatedDate();
                    case 2:
                        return ordermaterial.getTotalprice();
                    case 3:
                        return ordermaterial.getEmid();

                    default:
                        return "Unknown";
                }
            }
        });

        ordermaterialdService = new OrderMaterialService();
        listd = ordermaterialdService.getOrderMaterialDetail();
        tblOrdermaterialdetail.setRowHeight(100);
        tblOrdermaterialdetail.setModel(new AbstractTableModel() {
            String[] columnNames = {"Order material detail ID", "Unit", "Price", "Order material ID", "Materiall ID"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return listd.size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                OrderMaterialDetail ordermaterialdetail = listd.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return ordermaterialdetail.getOrdermaterialdetailId();
                    case 1:
                        return ordermaterialdetail.getOrdermaterialdetailUnit();
                    case 2:
                        return ordermaterialdetail.getOrdermaterialdetailPrice();
                    case 3:
                        return ordermaterialdetail.getOrdermaterialId();
                    case 4:
                        return ordermaterialdetail.getMaterialId();

                    default:
                        return "Unknown";
                }
            }
        });

        tblOrdermaterial.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
//                txtomid.setEnabled(true);
                int row = tblOrdermaterial.rowAtPoint(e.getPoint());
                int col = tblOrdermaterial.columnAtPoint(e.getPoint());
                OrderMaterial ordermaterial = list.get(row);
                editedOrderMaterial = ordermaterial;
                String omid = Integer.toString(ordermaterial.getId());
                txtomid.setText(omid);
                String timeString;
                String omtp = Float.toString(ordermaterial.getTotalprice());
                txtomtp.setText(omtp);
                String emid = Integer.toString(ordermaterial.getEmid());
                txtepid.setText(emid);
                txtTime.setText(ordermaterial.getCreatedDate() + "");
                setObjectToForm();

            }

        });
        tblOrdermaterialdetail.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                int rowd = tblOrdermaterialdetail.rowAtPoint(e.getPoint());
                int cold = tblOrdermaterialdetail.columnAtPoint(e.getPoint());

//                System.out.println(listd.get(rowd));
                ordermaterialdetail = listd.get(rowd);

                String omdid = Integer.toString(ordermaterialdetail.getOrdermaterialdetailId());
                txtomdid.setText(omdid);

                String omdu = Integer.toString(ordermaterialdetail.getOrdermaterialdetailUnit());
                txtomdu.setText(omdu);
                String omdp = Float.toString(ordermaterialdetail.getOrdermaterialdetailPrice());
                txtomdp.setText(omdp);
                String mid2 = Integer.toString(ordermaterialdetail.getOrdermaterialId());;
                txtomid2.setText(mid2);
                String mid = Integer.toString(ordermaterialdetail.getMaterialId());
                txtmid.setText(mid);
                setObjectToForm();
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        omdid = new javax.swing.JLabel();
        omdu = new javax.swing.JLabel();
        omdp = new javax.swing.JLabel();
        omid2 = new javax.swing.JLabel();
        mid = new javax.swing.JLabel();
        txtomdid = new javax.swing.JTextField();
        txtomdu = new javax.swing.JTextField();
        txtomdp = new javax.swing.JTextField();
        txtomid2 = new javax.swing.JTextField();
        txtmid = new javax.swing.JTextField();
        btnSave2 = new javax.swing.JButton();
        btnClear2 = new javax.swing.JButton();
        btnDelete2 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        omid = new javax.swing.JLabel();
        omtp = new javax.swing.JLabel();
        epid = new javax.swing.JLabel();
        txtomid = new javax.swing.JTextField();
        txtomtp = new javax.swing.JTextField();
        txtepid = new javax.swing.JTextField();
        btnSave = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        txtTime = new javax.swing.JTextField();
        Time = new javax.swing.JLabel();
        btnDelete = new javax.swing.JButton();
        Ordermaterial = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblOrdermaterialdetail = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblOrdermaterial = new javax.swing.JTable();
        Ordermaterialdetail = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(96, 68, 34));

        jPanel4.setBackground(new java.awt.Color(96, 68, 34));

        jPanel2.setBackground(new java.awt.Color(211, 184, 173));

        omdid.setForeground(new java.awt.Color(51, 51, 51));
        omdid.setText("Order Material Detail ID");

        omdu.setForeground(new java.awt.Color(51, 51, 51));
        omdu.setText("Order Material Detail Unit");

        omdp.setForeground(new java.awt.Color(51, 51, 51));
        omdp.setText("Order Materil Detail Price");

        omid2.setForeground(new java.awt.Color(51, 51, 51));
        omid2.setText("Order Material ID");

        mid.setForeground(new java.awt.Color(51, 51, 51));
        mid.setText("Material ID");

        txtomdid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtomdidActionPerformed(evt);
            }
        });

        btnSave2.setText("Save");
        btnSave2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSave2ActionPerformed(evt);
            }
        });

        btnClear2.setText("Clear");
        btnClear2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClear2ActionPerformed(evt);
            }
        });

        btnDelete2.setText("Delete");
        btnDelete2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelete2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnSave2)
                        .addGap(18, 18, 18)
                        .addComponent(btnClear2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnDelete2)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(omdid)
                                .addGap(42, 42, 42))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(omdu)
                                    .addComponent(omdp)
                                    .addComponent(omid2)
                                    .addComponent(mid))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtomdid, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtmid, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtomid2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtomdu, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtomdp, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtomdid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(omdid))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(omdu)
                    .addComponent(txtomdu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(omdp)
                    .addComponent(txtomdp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(omid2)
                    .addComponent(txtomid2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtmid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mid))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave2)
                    .addComponent(btnClear2)
                    .addComponent(btnDelete2))
                .addContainerGap())
        );

        jPanel1.setBackground(new java.awt.Color(211, 184, 173));

        omid.setForeground(new java.awt.Color(51, 51, 51));
        omid.setText("Order Material ID");

        omtp.setForeground(new java.awt.Color(51, 51, 51));
        omtp.setText("Order Material Total Price");

        epid.setForeground(new java.awt.Color(51, 51, 51));
        epid.setText("Employee ID");

        txtomid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtomidActionPerformed(evt);
            }
        });

        txtomtp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtomtpActionPerformed(evt);
            }
        });

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        txtTime.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTimeActionPerformed(evt);
            }
        });

        Time.setForeground(new java.awt.Color(51, 51, 51));
        Time.setText("Time");

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnSave)
                        .addGap(18, 18, 18)
                        .addComponent(btnClear)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnDelete)
                        .addGap(12, 12, 12))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(omid)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtomid, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(omtp)
                            .addComponent(epid))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtomtp, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                            .addComponent(txtepid, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(Time, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                        .addComponent(txtTime, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(omid, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtomid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(omtp)
                    .addComponent(txtomtp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(epid)
                    .addComponent(txtepid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Time, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 116, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave)
                    .addComponent(btnClear)
                    .addComponent(btnDelete))
                .addContainerGap())
        );

        Ordermaterial.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        Ordermaterial.setForeground(new java.awt.Color(255, 255, 255));
        Ordermaterial.setText("                ORDER MATERIAL");

        tblOrdermaterialdetail.setBackground(new java.awt.Color(210, 185, 167));
        tblOrdermaterialdetail.setForeground(new java.awt.Color(51, 51, 51));
        tblOrdermaterialdetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5"
            }
        ));
        jScrollPane3.setViewportView(tblOrdermaterialdetail);

        tblOrdermaterial.setAutoCreateRowSorter(true);
        tblOrdermaterial.setBackground(new java.awt.Color(210, 185, 167));
        tblOrdermaterial.setForeground(new java.awt.Color(51, 51, 51));
        tblOrdermaterial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblOrdermaterial);

        Ordermaterialdetail.setBackground(new java.awt.Color(255, 255, 255));
        Ordermaterialdetail.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        Ordermaterialdetail.setForeground(new java.awt.Color(255, 255, 255));
        Ordermaterialdetail.setText("           ORDER MATERIAL DETAIL");

        jButton1.setFont(new java.awt.Font("Segoe UI", 0, 30)); // NOI18N
        jButton1.setText("Back");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addComponent(Ordermaterialdetail, javax.swing.GroupLayout.PREFERRED_SIZE, 745, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(Ordermaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 735, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(487, 487, 487)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jScrollPane1)
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 998, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(12, 12, 12)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 335, Short.MAX_VALUE))))))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(Ordermaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Ordermaterialdetail, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 302, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 23, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void txtomidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtomidActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtomidActionPerformed

    private void txtomtpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtomtpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtomtpActionPerformed

    private void txtomdidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtomdidActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtomdidActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed

        OrderMaterial ordermaterial;
        System.out.println(editedOrderMaterial);
        if (editedOrderMaterial.getId() < 0) {//Add New
            setFormToObject();
            ordermaterial = ordermaterialService.addNew(editedOrderMaterial);
        } else {
            setFormToObject();
            ordermaterial = ordermaterialService.update(editedOrderMaterial);
        }
        refreshTxtandtable();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        refreshTxtandtable();

    }//GEN-LAST:event_btnClearActionPerformed

    private void txtTimeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTimeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTimeActionPerformed

    private void btnSave2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSave2ActionPerformed
//        cal();
        OrderMaterialDetail orderMaterialDetail;
        OrderMaterial ordermaterial;
        OrderMaterialService orderMaterialService = new OrderMaterialService();
        System.out.println("opject555555555555" + editedOrderMaterialDetail);
        if (editedOrderMaterialDetail.getOrdermaterialdetailId() < 0) {//Add New
            setFormToObjectDetail();
            orderMaterialDetail = ordermaterialService.addNew(editedOrderMaterialDetail);
        } else {
            setFormToObjectDetail();
            orderMaterialDetail = ordermaterialService.updateDetail(editedOrderMaterialDetail);
        }
        cal();

        ordermaterial = orderMaterialService.getById(orderMaterialDetail.getOrdermaterialId());
        ordermaterial.setTotalprice(ordermaterial.getTotalprice() + total);
        orderMaterialService.update(ordermaterial);

        refreshTxtandtable();
        refreshTxtandtableDetail();
    }//GEN-LAST:event_btnSave2ActionPerformed

    private void btnDelete2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelete2ActionPerformed

        OrderMaterial ordermaterial;
        OrderMaterialService orderMaterialService = new OrderMaterialService();

        
        ordermaterial = orderMaterialService.getById(ordermaterialdetail.getOrdermaterialId());
        ordermaterial.setTotalprice(ordermaterial.getTotalprice() - ordermaterialdetail.getOrdermaterialdetailPrice());
        orderMaterialService.update(ordermaterial);
        ordermaterialdService.deleteDetail(ordermaterialdetail);

        refreshTxtandtable();
        refreshTxtandtableDetail();

    }//GEN-LAST:event_btnDelete2ActionPerformed

    private void btnClear2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClear2ActionPerformed
        refreshTxtandtableDetail();
    }//GEN-LAST:event_btnClear2ActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedIndex = tblOrdermaterial.getSelectedRow();
        if(selectedIndex>=0){
            editedOrderMaterial = list.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if(input==0){
                ordermaterialService.delete(editedOrderMaterial);
            }
        refreshTxtandtable();
        refreshTxtandtableDetail();
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void cal() {
        total = 0;
        float price = Float.parseFloat(txtomdp.getText());
        total = total + price;
    }

    private void refreshTxtandtable() {
        list = ordermaterialService.getOrderMaterial();
        tblOrdermaterial.revalidate();
        tblOrdermaterial.repaint();

        tblOrdermaterial.getSelectionModel().clearSelection();
        txtomid.setText("");
        txtomtp.setText("");
        txtepid.setText("");
        txtTime.setText("");
        editedOrderMaterial.setId(-1);
    }

    private void refreshTxtandtableDetail() {
        listd = ordermaterialService.getOrderMaterialDetail();
        tblOrdermaterialdetail.revalidate();
        tblOrdermaterialdetail.repaint();

        tblOrdermaterialdetail.getSelectionModel().clearSelection();
        txtomdid.setText("");
        txtomdu.setText("");
        txtomdp.setText("");
        txtomid2.setText("");
        txtmid.setText("");
        editedOrderMaterialDetail.setOrdermaterialdetailId(-1);
    }

    private void setFormToObject() {
        if (!txtomid.getText().equals("")) {
            int iomid = Integer.parseInt(txtomid.getText());
            editedOrderMaterial.setId(iomid);
        }
        if (!txtomtp.getText().equals("")) {
            float iomtp = Float.parseFloat(txtomtp.getText());
            editedOrderMaterial.setTotalprice(iomtp);
        } else {
            float i = 0;
            editedOrderMaterial.setTotalprice(i);
        }
//        float iomtp = Float.parseFloat(txtomtp.getText());
//        editedOrderMaterial.setTotalprice(iomtp);
        int iemid = Integer.parseInt(txtepid.getText());
        editedOrderMaterial.setEmid(iemid);
    }

    private void setFormToObjectDetail() {
        if (!txtomdid.getText().equals("")) {
            int itxtomdid = Integer.parseInt(txtomdid.getText());
            editedOrderMaterialDetail.setOrdermaterialdetailId(itxtomdid);

        }

        if (!txtomdu.getText().equals("")) {
            int itxtomdu = Integer.parseInt(txtomdu.getText());
            editedOrderMaterialDetail.setOrdermaterialdetailUnit(itxtomdu);
        }

        if (!txtomdp.getText().equals("")) {
            float itxtomdp = Float.parseFloat(txtomdp.getText());
            editedOrderMaterialDetail.setOrdermaterialdetailPrice(itxtomdp);
        }
        if (!txtomid2.getText().equals("")) {
            int itxtomid2 = Integer.parseInt(txtomid2.getText());
            editedOrderMaterialDetail.setOrdermaterialId(itxtomid2);
        }
        if (!txtmid.getText().equals("")) {
            int itxtmid = Integer.parseInt(txtmid.getText());
            editedOrderMaterialDetail.setMaterialId(itxtmid);
        }

    }

    private void setObjectToForm() {
//        txtomid.setText(editedOrderMaterial.getId()+"");
//        txtomtp.setText(editedOrderMaterial.getTotalprice()+"");
//        txtepid.setText(editedOrderMaterial.getEmid()+"");

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Order.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Order.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Order.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Order.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Order().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Ordermaterial;
    private javax.swing.JLabel Ordermaterialdetail;
    private javax.swing.JLabel Time;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnClear2;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnDelete2;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSave2;
    private javax.swing.JLabel epid;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel mid;
    private javax.swing.JLabel omdid;
    private javax.swing.JLabel omdp;
    private javax.swing.JLabel omdu;
    private javax.swing.JLabel omid;
    private javax.swing.JLabel omid2;
    private javax.swing.JLabel omtp;
    private javax.swing.JTable tblOrdermaterial;
    private javax.swing.JTable tblOrdermaterialdetail;
    private javax.swing.JTextField txtTime;
    private javax.swing.JTextField txtepid;
    private javax.swing.JTextField txtmid;
    private javax.swing.JTextField txtomdid;
    private javax.swing.JTextField txtomdp;
    private javax.swing.JTextField txtomdu;
    private javax.swing.JTextField txtomid;
    private javax.swing.JTextField txtomid2;
    private javax.swing.JTextField txtomtp;
    // End of variables declaration//GEN-END:variables
}
