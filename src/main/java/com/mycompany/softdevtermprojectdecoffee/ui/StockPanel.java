/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.ui;
import com.mycompany.softdevtermprojectdecoffee.model.Material;
import com.mycompany.softdevtermprojectdecoffee.service.CheckstockService;
import com.mycompany.softdevtermprojectdecoffee.service.EmployeeService;
import com.mycompany.softdevtermprojectdecoffee.service.MaterialService;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author boat5
 */
public class StockPanel extends javax.swing.JPanel {
    private final MaterialService materialService;
    private final CheckstockService checkstockService;
    private List<Material> list;
    private Material editedMaterial;
    private final AbstractTableModel model;
    /**
     * Creates new form StockPanel
     */
    public StockPanel() {
        initComponents();
        txtSearchMaterial.setToolTipText("Search Material");
        Font tahomaFont = loadText();
        materialService = new MaterialService();
        checkstockService = new CheckstockService();
        list = materialService.getMaterials();
        JTableHeader header = tblStock1.getTableHeader();
        header.setFont(tahomaFont);
        model = new AbstractTableModel() {
            String[] columnNames = {"ID", "Name", "Minimum", "Unit", "Type", "Price", "Total"};
            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }
            
            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 7;
            }
            
            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Material material = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return material.getId();
                    case 1:
                        return material.getName();
                    case 2:
                        return material.getMinimum();
                    case 3:
                        return material.getUnit();
                    case 4:
                        return material.getUnitType();
                    case 5:
                        return material.getPrice();
                    case 6:
                        return material.getPrice()*material.getUnit();
                    default:
                        return "Unknown";
                }
            }
        };
        tblStock1.setRowHeight(30);
        tblStock1.setModel(model);
        btnSearch.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String searchTerm = txtSearchMaterial.getText();
                        TableRowSorter sorter = new TableRowSorter(tblStock1.getModel());
                        sorter.setRowFilter(RowFilter.regexFilter(searchTerm));
                        tblStock1.setRowSorter(sorter);
                    }
                });
        Time(tahomaFont);
        btnPrint.setEnabled(false);
    }

    private Font loadText() {
        txtName.setText(EmployeeService.getCurrentUser().getName());
        txtName.setForeground(Color.WHITE);
        Font tahomaFont = new Font("Tahoma", Font.PLAIN, 18);
        txtName.setFont(tahomaFont);
        switch (EmployeeService.getCurrentUser().getRole()) {
            case 1 -> txtRole.setText("Barista");
            case 2 -> txtRole.setText("Worker");
            case 3 -> txtRole.setText("Cashier");
            case 4 -> txtRole.setText("CEO");
            default -> txtRole.setText("Unknown");
        }
        txtRole.setForeground(Color.WHITE);
        txtRole.setFont(tahomaFont);
        return tahomaFont;
    }

    private void refreshTable(){
        list = materialService.getMaterials();
        tblStock1.revalidate();
        tblStock1.repaint();
    }
    
    private void Time(Font tahomaFont){
        Timer timer = new Timer(100, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                    String formattedTime = LocalDateTime.now().format(formatter);
                    txtDate.setText(formattedTime);
                    txtDate.setForeground(Color.WHITE);
                    txtDate.setFont(tahomaFont);
                }
            });
        timer.start();
    }
    
    
    private void CheckUpdateStock(int late){
        if(checkstockService.getLatestId() == late){
            btnPrint.setEnabled(true);
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        StockPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblStock = new javax.swing.JTable();
        tblStock1 = new javax.swing.JTable();
        btnCheckStock = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        txtSearchMaterial = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();
        Date = new javax.swing.JLabel();
        Name = new javax.swing.JLabel();
        Role = new javax.swing.JLabel();
        txtDate = new javax.swing.JLabel();
        txtName = new javax.swing.JLabel();
        txtRole = new javax.swing.JLabel();
        btnOrder = new javax.swing.JButton();

        StockPanel.setBackground(new java.awt.Color(96, 68, 34));

        tblStock.setBackground(new java.awt.Color(241, 227, 205));
        tblStock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblStock);

        tblStock1.setBackground(new java.awt.Color(244, 214, 192));
        tblStock1.setForeground(new java.awt.Color(51, 51, 51));
        tblStock1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblStock1);

        btnCheckStock.setBackground(new java.awt.Color(117, 87, 59));
        btnCheckStock.setForeground(new java.awt.Color(255, 255, 255));
        btnCheckStock.setText("Check Stock");
        btnCheckStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckStockActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Yu Gothic UI", 1, 36)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("STOCK MENAGEMENT");

        txtSearchMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchMaterialActionPerformed(evt);
            }
        });
        txtSearchMaterial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchMaterialKeyPressed(evt);
            }
        });

        btnSearch.setBackground(new java.awt.Color(117, 87, 59));
        btnSearch.setForeground(new java.awt.Color(255, 255, 255));
        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnEdit.setBackground(new java.awt.Color(117, 87, 59));
        btnEdit.setForeground(new java.awt.Color(255, 255, 255));
        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnPrint.setBackground(new java.awt.Color(167, 131, 98));
        btnPrint.setText("Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        Date.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Date.setForeground(new java.awt.Color(255, 255, 255));
        Date.setText("Time :");

        Name.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Name.setForeground(new java.awt.Color(255, 255, 255));
        Name.setText("Name :");

        Role.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Role.setForeground(new java.awt.Color(255, 255, 255));
        Role.setText("Role :");

        txtDate.setBackground(new java.awt.Color(255, 255, 255));
        txtDate.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        txtName.setBackground(new java.awt.Color(255, 255, 255));
        txtName.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        txtRole.setBackground(new java.awt.Color(255, 255, 255));
        txtRole.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        btnOrder.setBackground(new java.awt.Color(117, 87, 59));
        btnOrder.setForeground(new java.awt.Color(255, 255, 255));
        btnOrder.setText("Order");
        btnOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOrderActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout StockPanelLayout = new javax.swing.GroupLayout(StockPanel);
        StockPanel.setLayout(StockPanelLayout);
        StockPanelLayout.setHorizontalGroup(
            StockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(StockPanelLayout.createSequentialGroup()
                .addGroup(StockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(StockPanelLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 458, Short.MAX_VALUE)
                        .addComponent(Date)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDate, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Name)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(Role)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtRole, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(StockPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCheckStock)
                        .addGap(18, 18, 18)
                        .addComponent(txtSearchMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addComponent(jScrollPane2)
        );
        StockPanelLayout.setVerticalGroup(
            StockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(StockPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(StockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addGroup(StockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(txtName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(Name, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(StockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(txtDate, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(Date, javax.swing.GroupLayout.Alignment.LEADING))
                    .addGroup(StockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(txtRole, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(Role, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(34, 34, 34)
                .addGroup(StockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCheckStock, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSearchMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 812, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(StockPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(StockPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed

    }//GEN-LAST:event_btnSearchActionPerformed
    
    private void txtSearchMaterialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchMaterialKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchMaterialKeyPressed

    private void txtSearchMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchMaterialActionPerformed

    }//GEN-LAST:event_txtSearchMaterialActionPerformed

    private void btnCheckStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckStockActionPerformed
        int Latest = checkstockService.getLatestId()+1;
        editedMaterial= new Material();
        openCheckDialog(Latest);
    }//GEN-LAST:event_btnCheckStockActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        editedMaterial= new Material();
        openMaterialDialog();
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        ReportCheckstock report = new ReportCheckstock();
        JFrame Frame = new JFrame("Low Material in Stock");
        Image iconWindow = Toolkit.getDefaultToolkit().getImage("./DEVTOPIA.png");
        Frame.setIconImage(iconWindow);
        Frame.add(report);
        Frame.setSize(600, 668);
        Frame.setVisible(true);
        Frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
                btnPrint.setEnabled(false);
            }
        });
    }//GEN-LAST:event_btnPrintActionPerformed

    private void btnOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOrderActionPerformed
        Order OrderFrame = new Order();
        OrderFrame.setVisible(true);
    }//GEN-LAST:event_btnOrderActionPerformed
    
    private void openCheckDialog(int Latest) {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        CheckMaterialDialog checkMaterialDialog = new CheckMaterialDialog(frame,editedMaterial);
        checkMaterialDialog.setLocationRelativeTo(this);
        checkMaterialDialog.setTitle("Check Stock");
        Image iconWindow = Toolkit.getDefaultToolkit().getImage("./DEVTOPIA.png");
        checkMaterialDialog.setIconImage(iconWindow);
        checkMaterialDialog.setVisible(true);
        checkMaterialDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
                CheckUpdateStock(Latest);
            }
        });
    }
    
    private void openMaterialDialog() {
        JFrame Crud = (JFrame) SwingUtilities.getRoot(this);
        MaterialCRUDDialog crudDialog = new MaterialCRUDDialog(Crud,editedMaterial);
        crudDialog.setLocationRelativeTo(this);
        crudDialog.setTitle("Material Management");
        Image iconWindow = Toolkit.getDefaultToolkit().getImage("./DEVTOPIA.png");
        crudDialog.setIconImage(iconWindow);
        crudDialog.setVisible(true);
        crudDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }
        });
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Date;
    private javax.swing.JLabel Name;
    private javax.swing.JLabel Role;
    private javax.swing.JPanel StockPanel;
    private javax.swing.JButton btnCheckStock;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnOrder;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnSearch;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblStock;
    private javax.swing.JTable tblStock1;
    private javax.swing.JLabel txtDate;
    private javax.swing.JLabel txtName;
    private javax.swing.JLabel txtRole;
    private javax.swing.JTextField txtSearchMaterial;
    // End of variables declaration//GEN-END:variables
}
