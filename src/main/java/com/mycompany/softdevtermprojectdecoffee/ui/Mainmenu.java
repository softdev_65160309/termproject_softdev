/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.ui;
import com.mycompany.softdevtermprojectdecoffee.model.Checkstock;
import com.mycompany.softdevtermprojectdecoffee.model.CheckstockDetail;
import com.mycompany.softdevtermprojectdecoffee.model.Employee;
import com.mycompany.softdevtermprojectdecoffee.model.Material;
import com.mycompany.softdevtermprojectdecoffee.model.OrderMaterial;
import com.mycompany.softdevtermprojectdecoffee.model.Product;
import com.mycompany.softdevtermprojectdecoffee.model.Receipt;
import com.mycompany.softdevtermprojectdecoffee.model.ReceiptDetail;
import com.mycompany.softdevtermprojectdecoffee.service.CheckstockService;
import com.mycompany.softdevtermprojectdecoffee.service.EmployeeService;
import com.mycompany.softdevtermprojectdecoffee.service.MaterialService;
import com.mycompany.softdevtermprojectdecoffee.service.OrderMaterialService;
import com.mycompany.softdevtermprojectdecoffee.service.ProductService;
import com.mycompany.softdevtermprojectdecoffee.service.ReceiptService;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;

/**
 *
 * @author boat5
 */
public class Mainmenu extends javax.swing.JPanel {
    private final CheckstockService checkstockService;
    private List<CheckstockDetail> list;
    private final MaterialService materialService;
    private final ReceiptService receiptService = new ReceiptService();
    private final EmployeeService employeeService = new EmployeeService();
    private final ProductService productService = new ProductService();
    private final OrderMaterialService OrdermaterialService = new OrderMaterialService();
    /*
     * Creates new form Mainmenu
     */
    public Mainmenu() {
        initComponents();
        refreshTable();
        int EmpId = EmployeeService.getCurrentUser().getId();
        loadImage(EmpId);
        loadText();
        checkstockService = new CheckstockService();
        materialService = new MaterialService();
        Font tahomaFont = new Font("Tahoma", Font.PLAIN, 18);
        JTableHeader header = tblMaterialLow.getTableHeader();
        JTableHeader header1 = tblBestSeller.getTableHeader();
        header.setFont(tahomaFont);
        header1.setFont(tahomaFont);
        List<Material> Materials = materialService.getMaterials();
        List<Material> lowStockMaterials = new ArrayList<>();
        for (int i = 0; i < Materials.size(); i++) {
            Material editedMaterial = Materials.get(i);
            if (editedMaterial.getUnit() < editedMaterial.getMinimum()) {
                lowStockMaterials.add(editedMaterial);
            }
        }
        tblMaterialLow.setModel(new AbstractTableModel() {
            String[] columnNames = {"Material", "Minimum", "Unit", "Price"};
            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }
            
            @Override
            public int getRowCount() {
                return lowStockMaterials.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }
            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Material materiallow = lowStockMaterials.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return materiallow.getName();
                    case 1: 
                        return materiallow.getMinimum();
                    case 2: 
                        return materiallow.getUnit();    
                    case 3:
                        return materiallow.getPrice();
                    default:
                        return "Unknown";
                }
            }
        });
        tblMaterialLow.setRowHeight(50);
        
        List<ReceiptDetail> receiptlist = receiptService.getByBestSeller();
        List<Receipt> receipt = receiptService.getReceipt();
        List<OrderMaterial> Cost = OrdermaterialService.getOrderMaterial();
        List<Product> productList = productService.getProducts();
        tblBestSeller.setModel(new AbstractTableModel() {
            String[] columnNames = {"Rank", "Product", "Category", "Volume", "Price"};
            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }
            
            @Override
            public int getRowCount() {
                return receiptlist.size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }
            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReceiptDetail productBest = receiptlist.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return rowIndex+1;
                    case 1: 
                        for (Product pro : productList) {
                            if (productBest.getProductId() == pro.getId()) {
                                return pro.getName();
                            }
                        }
                    case 2:
                        for (Product pro : productList) {
                            if (productBest.getProductId() == pro.getId()) {
                                int catId = pro.getCategoryId();
                                switch (catId) {
                                case 1:
                                    return "Coffee";
                                case 2:
                                    return "Dessert";
                                case 3:
                                    return "Drink";
                                default:
                                    return "Unknown";    
                                }
                            }
                        }    
                    case 3:
                        return productBest.getQty();
                    case 4:
                        for (Product pro : productList) {
                            if (productBest.getProductId() == pro.getId()) {
                                return pro.getPrice();
                            }
                        }
                    default:
                        return "Unknown";
                }
            }
        });
        float Profit = calProfit(receipt, Cost);
        tblBestSeller.setRowHeight(50);
        txtIncome.setText("Income : "+calIncome(receipt) + " Baht");
        txtExpense.setText("Expense : "+calExpense(Cost) + " Baht"); 
        txtProfit.setText("Profit : "+ Profit + " Baht");
        Color green = new java.awt.Color(46, 139, 87);
        Color red = new java.awt.Color(178,34,34);
        if(Profit > 0){
            txtProfit.setForeground(green);
        }if(Profit == 0){
            txtProfit.setForeground(Color.BLACK);
        }if(Profit < 0){
            txtProfit.setForeground(red);
        }
    }
    
    private float calIncome(List<Receipt> receipt) {
        float Income = 0;
        for(int i = 0; i < receipt.size(); i++){
            Receipt income = receipt.get(i);
            Income = Income + income.getTotal();
        }
        return Income;
    }
    
    private void refreshTable(){
        tblBestSeller.revalidate();
        tblBestSeller.repaint();
        tblMaterialLow.revalidate();
        tblMaterialLow.repaint();
    }
    
    
    private float calExpense(List<OrderMaterial> Cost) {
        float Expense = 0;
        for(int i = 0; i < Cost.size(); i++){
            OrderMaterial costOrder = Cost.get(i);
            Expense = Expense + costOrder.getTotalprice();
        }
        return Expense;
    }
    
    private float calProfit(List<Receipt> receipt, List<OrderMaterial> Cost) {
        float TotalProfit = 0;
        TotalProfit = calIncome(receipt) - calExpense(Cost);
        return TotalProfit;
    }

    private void loadText() {
        txtName.setText(EmployeeService.getCurrentUser().getName());
        txtName.setForeground(Color.WHITE);
        Font tahomaFont = new Font("Tahoma", Font.PLAIN, 18);
        txtName.setFont(tahomaFont);
        welName.setText(EmployeeService.getCurrentUser().getName());
        switch (EmployeeService.getCurrentUser().getRole()) {
            case 1 -> txtRole.setText("Barista");
            case 2 -> txtRole.setText("Worker");
            case 3 -> txtRole.setText("Cashier");
            case 4 -> txtRole.setText("CEO");
            default -> txtRole.setText("Unknown");
        }
        txtRole.setForeground(Color.WHITE);
        txtRole.setFont(tahomaFont);
        Time(tahomaFont);
    }
    
    

    private void loadImage(int EmpId) {
        ImageIcon icon = new ImageIcon("./employee_icon/"+"employee"+EmpId+".png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) (116 * ((float) width / height)), 116, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        lblPhoto.setIcon(icon);
    }
    
    private void Time(Font tahomaFont){
        Timer timer = new Timer(100, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                    String formattedTime = LocalDateTime.now().format(formatter);
                    txtDate.setText(formattedTime);
                    txtDate.setForeground(Color.WHITE);
                    txtDate.setFont(tahomaFont);
                }
            });
        timer.start();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        txtRole = new javax.swing.JLabel();
        txtName = new javax.swing.JLabel();
        txtDate = new javax.swing.JLabel();
        Role = new javax.swing.JLabel();
        Name = new javax.swing.JLabel();
        Date = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblMaterialLow = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblBestSeller = new javax.swing.JTable();
        txtExpense = new javax.swing.JLabel();
        BestSellerProducts1 = new javax.swing.JLabel();
        txtIncome = new javax.swing.JLabel();
        MaterialLow1 = new javax.swing.JLabel();
        txtProfit = new javax.swing.JLabel();
        lblPhoto = new javax.swing.JLabel();
        welName = new javax.swing.JLabel();
        Wel = new javax.swing.JLabel();

        jLabel1.setText("jLabel1");

        jPanel1.setBackground(new java.awt.Color(96, 68, 34));

        txtRole.setBackground(new java.awt.Color(255, 255, 255));
        txtRole.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        txtName.setBackground(new java.awt.Color(255, 255, 255));
        txtName.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        txtDate.setBackground(new java.awt.Color(255, 255, 255));
        txtDate.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        Role.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Role.setForeground(new java.awt.Color(255, 255, 255));
        Role.setText("Role :");

        Name.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Name.setForeground(new java.awt.Color(255, 255, 255));
        Name.setText("Name :");

        Date.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Date.setForeground(new java.awt.Color(255, 255, 255));
        Date.setText("Time :");

        jPanel2.setBackground(new java.awt.Color(241, 227, 205));

        tblMaterialLow.setBackground(new java.awt.Color(196, 140, 105));
        tblMaterialLow.setForeground(new java.awt.Color(51, 51, 51));
        tblMaterialLow.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblMaterialLow);

        tblBestSeller.setBackground(new java.awt.Color(196, 140, 105));
        tblBestSeller.setForeground(new java.awt.Color(51, 51, 51));
        tblBestSeller.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tblBestSeller);

        txtExpense.setFont(new java.awt.Font("Yu Gothic UI", 1, 36)); // NOI18N
        txtExpense.setForeground(new java.awt.Color(51, 51, 51));
        txtExpense.setText("Expense :");

        BestSellerProducts1.setFont(new java.awt.Font("Yu Gothic UI", 1, 36)); // NOI18N
        BestSellerProducts1.setForeground(new java.awt.Color(51, 51, 51));
        BestSellerProducts1.setText("Material Low");

        txtIncome.setFont(new java.awt.Font("Yu Gothic UI", 1, 36)); // NOI18N
        txtIncome.setForeground(new java.awt.Color(51, 51, 51));
        txtIncome.setText("Income :");

        MaterialLow1.setFont(new java.awt.Font("Yu Gothic UI", 1, 36)); // NOI18N
        MaterialLow1.setForeground(new java.awt.Color(51, 51, 51));
        MaterialLow1.setText("Best Seller Products");

        txtProfit.setFont(new java.awt.Font("Yu Gothic UI", 1, 36)); // NOI18N
        txtProfit.setForeground(new java.awt.Color(51, 51, 51));
        txtProfit.setText("Profit : ");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 625, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(160, 160, 160)
                        .addComponent(MaterialLow1)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 119, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtProfit, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtExpense, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtIncome, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(41, 41, 41))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(BestSellerProducts1)
                        .addGap(308, 308, 308))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 700, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(34, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(MaterialLow1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(BestSellerProducts1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(txtIncome)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtExpense)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtProfit)
                .addGap(34, 34, 34))
        );

        welName.setFont(new java.awt.Font("Yu Gothic UI", 1, 36)); // NOI18N
        welName.setForeground(new java.awt.Color(255, 255, 255));
        welName.setText(" ");

        Wel.setFont(new java.awt.Font("Yu Gothic UI", 1, 36)); // NOI18N
        Wel.setForeground(new java.awt.Color(255, 255, 255));
        Wel.setText("Welcome");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(lblPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(Date)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDate, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Name)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(Role)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtRole, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addComponent(Wel)
                        .addGap(18, 18, 18)
                        .addComponent(welName, javax.swing.GroupLayout.PREFERRED_SIZE, 301, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(Date, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Name, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtRole, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Role, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(7, 7, 7)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Wel)
                            .addComponent(welName)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(lblPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel BestSellerProducts1;
    private javax.swing.JLabel Date;
    private javax.swing.JLabel MaterialLow1;
    private javax.swing.JLabel Name;
    private javax.swing.JLabel Role;
    private javax.swing.JLabel Wel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblPhoto;
    private javax.swing.JTable tblBestSeller;
    private javax.swing.JTable tblMaterialLow;
    private javax.swing.JLabel txtDate;
    private javax.swing.JLabel txtExpense;
    private javax.swing.JLabel txtIncome;
    private javax.swing.JLabel txtName;
    private javax.swing.JLabel txtProfit;
    private javax.swing.JLabel txtRole;
    private javax.swing.JLabel welName;
    // End of variables declaration//GEN-END:variables
}
