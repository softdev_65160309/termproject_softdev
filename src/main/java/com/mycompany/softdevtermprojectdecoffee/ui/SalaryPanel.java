/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.ui;

import com.mycompany.softdevtermprojectdecoffee.service.SalaryService;
import java.util.List;
import com.mycompany.softdevtermprojectdecoffee.model.Salary;
import java.awt.Font;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
/**
 *
 * @author HP
 */
public class SalaryPanel extends javax.swing.JPanel {
    private SalaryService salaryService;
    private List<Salary> list;
    
    /**
     * Creates new form Salary
     */
    public SalaryPanel() {
        initComponents();
        setSize(1920,1080);
        salaryService = new SalaryService();
        list = salaryService.getSalarys();
        Font tahomaFont = new Font("Tahoma", Font.PLAIN, 18);
        JTableHeader header = tblSalary.getTableHeader();
        header.setFont(tahomaFont);
        tblSalary.setRowHeight(100);
        tblSalary.setModel(new AbstractTableModel(){
            String[] headers = {"ID", "Amount", "Givetime", "PaymentMethod", "Employee_id"};
            
            @Override
            public String getColumnName(int column) {
                return headers[column];
            }
            
            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Salary salary = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return salary.getId();
                    case 1:
                        return salary.getAmount();
                    case 2:
                        return salary.getGivetime();
                    case 3:
                        return salary.getPaymentmethod();
                    case 4:
                        return salary.getEmployeeId();
                    default:
                        return "Unknown";
                }
            }
            
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblSalary = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(224, 189, 150));

        tblSalary.setBackground(new java.awt.Color(234, 220, 204));
        tblSalary.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblSalary);

        jLabel1.setFont(new java.awt.Font("Yu Gothic UI", 0, 24)); // NOI18N
        jLabel1.setText("Summary Salary");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 749, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(43, 43, 43)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 429, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblSalary;
    // End of variables declaration//GEN-END:variables
}
