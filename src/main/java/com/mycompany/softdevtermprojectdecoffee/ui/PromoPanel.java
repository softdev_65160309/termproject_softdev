package com.mycompany.softdevtermprojectdecoffee;

import com.mycompany.softdevtermprojectdecoffee.model.Promotion;
import com.mycompany.softdevtermprojectdecoffee.model.PromotionDetail;
import com.mycompany.softdevtermprojectdecoffee.service.PromotionService;
import com.mycompany.softdevtermprojectdecoffee.ui.PromoDialog;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

public class PromoPanel extends javax.swing.JFrame {

    private PromotionService Promo;
    private List<Promotion> list;
    private List<PromotionDetail> list1;
    private Promotion editedPromotion;
    private PromotionDetail editedPromotionDetail;
    private Boolean d;

    public PromoPanel(java.awt.Frame parent) {
        initComponents();
        setSize(1600, 900);
        Promo = new PromotionService();
        list = Promo.getPromotion();
        list1 = Promo.getPromotionDetail();
        tblPromotion.setRowHeight(100);
        tblPromotion.setModel(new AbstractTableModel() {
            String[] headers = {"ID", "Name", "StartDate", "EndDate", "Discount(%)", "Condition", "Status"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 7;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Promotion promotion = list.get(rowIndex);
                PromotionDetail promotionDetail = list1.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return promotion.getPromotionId();
                    case 1:
                        return promotion.getPromotionName();
                    case 2:
                        return promotion.getPromotionstartDate();
                    case 3:
                        return promotion.getPromotionendDate();
                    case 4:
                        return promotionDetail.getPromotionDetailDiscount();
                    case 5:
                        return promotionDetail.getPromotionDetailCondition();
                    case 6:
                        return promotion.getPromotionStatus();
                    default:
                        return "Unknown";
                }
            }

        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPromotion = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setSize(new java.awt.Dimension(1600, 900));

        jPanel1.setBackground(new java.awt.Color(103, 81, 60));

        jLabel1.setFont(new java.awt.Font("Yu Gothic UI", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("PROMOTION");
        jLabel1.setToolTipText("");

        tblPromotion.setBackground(new java.awt.Color(210, 185, 167));
        tblPromotion.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblPromotion.setForeground(new java.awt.Color(51, 51, 51));
        tblPromotion.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblPromotion);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(697, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(686, 686, 686))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 63, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 773, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void refreshTable() {
        list = Promo.getPromotion();
        tblPromotion.revalidate();
        tblPromotion.repaint();
    }  
        private void openDialog(boolean a) {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        this.d = a;
        PromoDialog promoDialog = new PromoDialog(frame,editedPromotion,editedPromotionDetail,d);
        promoDialog.setLocationRelativeTo(this);
        promoDialog.setVisible(true);
        promoDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblPromotion;
    // End of variables declaration//GEN-END:variables
}
