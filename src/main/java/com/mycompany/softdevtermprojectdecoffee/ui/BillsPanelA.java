
package com.mycompany.softdevtermprojectdecoffee.ui;

import com.mycompany.softdevtermprojectdecoffee.model.Bills;
import com.mycompany.softdevtermprojectdecoffee.service.BillsService;
import com.mycompany.softdevtermprojectdecoffee.service.EmployeeService;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;

public class BillsPanelA extends javax.swing.JPanel {
    
    private BillsService bs;
    private List<Bills> list;
    private Bills editedBills;


    public BillsPanelA() {
        initComponents();
        setSize(1600, 900);
        Font tahomaFont = loadText();
        bs = new BillsService();
        list = bs.getBills();
        JTableHeader header = tblBills.getTableHeader();
        header.setFont(tahomaFont);
        tblBills.setRowHeight(100);
        tblBills.setModel(new AbstractTableModel() {
            String[] headers = {"ID", "Water Cost(Baht)", "Electric Cost(Baht)", "Paydate","Water(Unit):Baht","Electric(Unit):Baht","VAT","Used Water(Unit)","Used Electric(Unit)"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 9;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Bills Bills = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return Bills.getId();
                    case 1:
                        return Bills.getWater();
                    case 2:
                        return Bills.getElectric();
                    case 3:
                        return Bills.getPaydate();
                     case 4:
                     return Bills.getWtb();
                        case 5:
                        return Bills.getEtb();
                        case 6:
                        return Bills.getVat();
                        case 7:
                        return Bills.getUsedwater();
                        case 8:
                        return Bills.getUsedelectric();
                    default:
                        return "Unknown";
                }
            }

        });
        Time(tahomaFont);
    }

    private Font loadText() {
        txtName.setText(EmployeeService.getCurrentUser().getName());
        txtName.setForeground(Color.WHITE);
        Font tahomaFont = new Font("Tahoma", Font.PLAIN, 18);
        txtName.setFont(tahomaFont);
        switch (EmployeeService.getCurrentUser().getRole()) {
            case 1 -> txtRole.setText("Barista");
            case 2 -> txtRole.setText("Worker");
            case 3 -> txtRole.setText("Cashier");
            case 4 -> txtRole.setText("CEO");
            default -> txtRole.setText("Unknown");
        }
        txtRole.setForeground(Color.WHITE);
        txtRole.setFont(tahomaFont);
        return tahomaFont;
    }
    
    private void Time(Font tahomaFont) {
        Timer timer = new Timer(100, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                String formattedTime = LocalDateTime.now().format(formatter);
                txtDate.setText(formattedTime);
                txtDate.setForeground(Color.WHITE);
                txtDate.setFont(tahomaFont);
            }
        });
        timer.start();
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblBills = new javax.swing.JTable();
        Date = new javax.swing.JLabel();
        txtDate = new javax.swing.JLabel();
        Name = new javax.swing.JLabel();
        txtName = new javax.swing.JLabel();
        Role = new javax.swing.JLabel();
        txtRole = new javax.swing.JLabel();

        setBackground(new java.awt.Color(96, 68, 34));
        setPreferredSize(new java.awt.Dimension(1600, 1000));

        jLabel1.setFont(new java.awt.Font("Yu Gothic UI", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("BILLS");
        jLabel1.setToolTipText("");

        tblBills.setBackground(new java.awt.Color(210, 185, 167));
        tblBills.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblBills.setForeground(new java.awt.Color(51, 51, 51));
        tblBills.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblBills);

        Date.setFont(new java.awt.Font("Yu Gothic UI", 1, 18)); // NOI18N
        Date.setForeground(new java.awt.Color(255, 255, 255));
        Date.setText("Time :");

        txtDate.setBackground(new java.awt.Color(255, 255, 255));
        txtDate.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        Name.setFont(new java.awt.Font("Yu Gothic UI", 1, 18)); // NOI18N
        Name.setForeground(new java.awt.Color(255, 255, 255));
        Name.setText("Name :");

        txtName.setBackground(new java.awt.Color(255, 255, 255));
        txtName.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        Role.setFont(new java.awt.Font("Yu Gothic UI", 1, 18)); // NOI18N
        Role.setForeground(new java.awt.Color(255, 255, 255));
        Role.setText("Role :");

        txtRole.setBackground(new java.awt.Color(255, 255, 255));
        txtRole.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1583, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Date)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDate, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Name)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(Role)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtRole, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(Role, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(Name, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(Date, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtRole, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtDate, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 102, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 837, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Date;
    private javax.swing.JLabel Name;
    private javax.swing.JLabel Role;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblBills;
    private javax.swing.JLabel txtDate;
    private javax.swing.JLabel txtName;
    private javax.swing.JLabel txtRole;
    // End of variables declaration//GEN-END:variables
}
