/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.ui;

import com.mycompany.softdevtermprojectdecoffee.component.BuyProductable;
import com.mycompany.softdevtermprojectdecoffee.component.ProductListPanel;
import com.mycompany.softdevtermprojectdecoffee.model.Employee;
import com.mycompany.softdevtermprojectdecoffee.model.Member;
import com.mycompany.softdevtermprojectdecoffee.model.Product;
import com.mycompany.softdevtermprojectdecoffee.model.Promotion;
import com.mycompany.softdevtermprojectdecoffee.model.PromotionDetail;
import com.mycompany.softdevtermprojectdecoffee.model.Receipt;
import com.mycompany.softdevtermprojectdecoffee.model.ReceiptDetail;
import com.mycompany.softdevtermprojectdecoffee.service.EmployeeService;
import com.mycompany.softdevtermprojectdecoffee.service.MemberService;
import com.mycompany.softdevtermprojectdecoffee.service.ProductService;
import com.mycompany.softdevtermprojectdecoffee.service.PromotionService;
import com.mycompany.softdevtermprojectdecoffee.service.ReceiptService;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Sabusi
 */
public class PosPanel extends javax.swing.JPanel implements BuyProductable {

    private MainFrame mainFrame;
    ArrayList<Product> products;
    ProductService productService = new ProductService();
    ReceiptService recieptService = new ReceiptService();
    Receipt reciept;
    private final ProductListPanel productListPanel;
    private Member editedMember;
    private MemberService memberService;
    private Promotion promotion;
    private PromotionService promotionService;
    private PromotionDetail promotionDetail;
    float discountPoint = 0;
    float discountPromotion = 0;
    int receivePoint = 0;

    /**
     * Creates new form PosPanel
     */
    public PosPanel(MainFrame mainFrame) {

        this.mainFrame = mainFrame;
        initComponents();
        EmployeeService emps = new EmployeeService();

//        emps.login("Kitty_001", "aaaaaaa");
        txtName.setText(EmployeeService.getCurrentUser().getName());
        txtName.setForeground(Color.WHITE);
        Font tahomaFont = new Font("Tahoma", Font.PLAIN, 18); // You can adjust the size and style as needed

        txtName.setFont(tahomaFont);
        switch (EmployeeService.getCurrentUser().getRole()) {
            case 1 ->
                txtRole.setText("Barista");
            case 2 ->
                txtRole.setText("Worker");
            case 3 ->
                txtRole.setText("Cashier");
            case 4 ->
                txtRole.setText("CEO");
            default ->
                txtRole.setText("Unknown");
        }
        txtRole
                .setForeground(Color.WHITE);

        txtRole.setFont(tahomaFont);

        Time(tahomaFont);

        reciept = new Receipt();

        reciept.setEmployee(EmployeeService.getCurrentUser());
        promotionService = new PromotionService();
        memberService = new MemberService();

        tblRecieptDetail.setModel(
                new AbstractTableModel() {
            String[] headers = {"Name", "Price", "Qty", "Total"};

            @Override

            public int getRowCount() {
                return reciept.getReceiptDetails().size();
            }

            @Override
            public String getColumnName(int column
            ) {
                return headers[column];
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex
            ) {
                ArrayList<ReceiptDetail> recieptDetails = reciept.getReceiptDetails();
                ReceiptDetail recieptDetail = recieptDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return recieptDetail.getProductName();
                    case 1:
                        return recieptDetail.getProductPrice();
                    case 2:
                        return recieptDetail.getQty();
                    case 3:
                        return recieptDetail.getTotalPrice();
                    default:
                        return "";
                }
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex
            ) {
                ArrayList<ReceiptDetail> recieptDetails = reciept.getReceiptDetails();
                ReceiptDetail recieptDetail = recieptDetails.get(rowIndex);
                if (columnIndex == 2) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    recieptDetail.setQty(qty);
                    reciept.calculateTotal();
                    refreshReciept();
                    calDiscountPromotion();
                    refreshTotalDetail();

                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex
            ) {
                switch (columnIndex) {
                    case 2:
                        return true;
                    default:
                        return false;
                }
            }

        }
        );
        productListPanel = new ProductListPanel();

        productListPanel.addOnBuyProduct(
                this);
        scrProductList.setViewportView(productListPanel);

        txfCash.getDocument()
                .addDocumentListener(new DocumentListener() {
                    @Override
                    public void insertUpdate(DocumentEvent e
                    ) {
                        calChange();
                    }

                    @Override
                    public void removeUpdate(DocumentEvent e
                    ) {
                    }

                    @Override
                    public void changedUpdate(DocumentEvent e
                    ) {
                        calChange();
                    }

                    private void calChange() {
                        if (!txfCash.getText().isEmpty() || !txfCash.getText().equals("-")) {
                            float change = (Float.parseFloat(txfCash.getText()) - (reciept.getTotal() - calDiscount()));
                            lblChange.setText("" + change);
                        }
                    }
                }
                );
    }

    private void calDiscountPromotion() {
        if (editedMember != null) {
            promotionDetail = promotionService.getPromotionDetailByCondition(editedMember.getId());
            lblPromotion.setText("" + promotionDetail.getPromotionDetailDiscount());
        } else {
            promotionDetail = promotionService.getPromotionDetailByCondition(0);
            lblPromotion.setText("" + promotionDetail.getPromotionDetailDiscount());
        }
        discountPromotion = promotionDetail.getPromotionDetailDiscount();
        lblDiscount.setText("" + calDiscount());
    }

    private void refreshReciept() {
        tblRecieptDetail.revalidate();
        tblRecieptDetail.repaint();
        lblPrice.setText("" + reciept.getTotal());
        lblTotal.setText("" + (reciept.getTotal() - calDiscount()));
        calDiscountPromotion();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        Date = new javax.swing.JLabel();
        Name = new javax.swing.JLabel();
        Role = new javax.swing.JLabel();
        txtDate = new javax.swing.JLabel();
        txtName = new javax.swing.JLabel();
        txtRole = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        scrProductList = new javax.swing.JScrollPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblRecieptDetail = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        btnCalculate = new javax.swing.JButton();
        btnSearchMember = new javax.swing.JButton();
        btnAddMember = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        lblPrice = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        lblMemberName = new javax.swing.JLabel();
        lblPoint = new javax.swing.JLabel();
        lblTel = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        txfCash = new javax.swing.JTextField();
        lblChange = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lblDiscount = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        lblGetPoint = new javax.swing.JLabel();
        tbnUsePoint = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        lblPromotion = new javax.swing.JLabel();
        btnMainMenu = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        btnAll = new javax.swing.JButton();
        btnCoffee = new javax.swing.JButton();
        btnDrink = new javax.swing.JButton();
        btnDessert = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();

        jPanel1.setBackground(new java.awt.Color(96, 68, 34));

        Date.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Date.setForeground(new java.awt.Color(255, 255, 255));
        Date.setText("Time :");

        Name.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Name.setForeground(new java.awt.Color(255, 255, 255));
        Name.setText("Name :");

        Role.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Role.setForeground(new java.awt.Color(255, 255, 255));
        Role.setText("Role :");

        txtDate.setBackground(new java.awt.Color(255, 255, 255));
        txtDate.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        txtName.setBackground(new java.awt.Color(255, 255, 255));
        txtName.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        txtRole.setBackground(new java.awt.Color(255, 255, 255));
        txtRole.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(811, Short.MAX_VALUE)
                .addComponent(Date)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDate, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Name)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Role)
                .addGap(12, 12, 12)
                .addComponent(txtRole, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Name, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtRole, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Role, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Date, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(176, 138, 105));

        scrProductList.setForeground(new java.awt.Color(51, 51, 51));
        scrProductList.setFont(new java.awt.Font("Segoe UI", 0, 8)); // NOI18N

        tblRecieptDetail.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblRecieptDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblRecieptDetail);

        btnCalculate.setBackground(new java.awt.Color(102, 255, 102));
        btnCalculate.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnCalculate.setForeground(new java.awt.Color(255, 255, 255));
        btnCalculate.setText("Caculate");
        btnCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalculateActionPerformed(evt);
            }
        });

        btnSearchMember.setBackground(new java.awt.Color(148, 115, 171));
        btnSearchMember.setFont(new java.awt.Font("Yu Gothic UI", 0, 14)); // NOI18N
        btnSearchMember.setForeground(new java.awt.Color(255, 255, 255));
        btnSearchMember.setText("Search Member");
        btnSearchMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchMemberActionPerformed(evt);
            }
        });

        btnAddMember.setBackground(new java.awt.Color(144, 112, 93));
        btnAddMember.setFont(new java.awt.Font("Yu Gothic UI", 0, 14)); // NOI18N
        btnAddMember.setForeground(new java.awt.Color(255, 255, 255));
        btnAddMember.setText("Add Member");
        btnAddMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddMemberActionPerformed(evt);
            }
        });

        btnCancle.setBackground(new java.awt.Color(255, 102, 102));
        btnCancle.setFont(new java.awt.Font("Yu Gothic UI", 0, 14)); // NOI18N
        btnCancle.setForeground(new java.awt.Color(255, 255, 255));
        btnCancle.setText("Cancel");
        btnCancle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancleActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnSearchMember, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                    .addComponent(btnCancle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnAddMember, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                    .addComponent(btnCalculate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSearchMember, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAddMember, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnCancle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCalculate, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE))
                .addGap(21, 21, 21))
        );

        jPanel4.setBackground(new java.awt.Color(194, 169, 145));

        lblPrice.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblPrice.setForeground(new java.awt.Color(71, 71, 71));
        lblPrice.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPrice.setText("-");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(71, 71, 71));
        jLabel1.setText("Member : ");

        lblMemberName.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblMemberName.setForeground(new java.awt.Color(71, 71, 71));
        lblMemberName.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMemberName.setText("-");

        lblPoint.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblPoint.setForeground(new java.awt.Color(71, 71, 71));
        lblPoint.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPoint.setText("-");

        lblTel.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblTel.setForeground(new java.awt.Color(71, 71, 71));
        lblTel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTel.setText("-");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(71, 71, 71));
        jLabel3.setText("Tel :");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(71, 71, 71));
        jLabel4.setText("Point :");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(71, 71, 71));
        jLabel5.setText("Price :");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(71, 71, 71));
        jLabel6.setText("Total :");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(71, 71, 71));
        jLabel7.setText("Cash :");

        lblTotal.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        lblTotal.setForeground(new java.awt.Color(71, 71, 71));
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTotal.setText("-");

        txfCash.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txfCash.setForeground(new java.awt.Color(204, 204, 204));
        txfCash.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        lblChange.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblChange.setForeground(new java.awt.Color(71, 71, 71));
        lblChange.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblChange.setText("-");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(71, 71, 71));
        jLabel9.setText("Change :");

        lblDiscount.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblDiscount.setForeground(new java.awt.Color(71, 71, 71));
        lblDiscount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDiscount.setText("-");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(71, 71, 71));
        jLabel10.setText("Discount :");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(71, 71, 71));
        jLabel11.setText("Get point :");

        lblGetPoint.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblGetPoint.setForeground(new java.awt.Color(71, 71, 71));
        lblGetPoint.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblGetPoint.setText("-");

        tbnUsePoint.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        tbnUsePoint.setForeground(new java.awt.Color(204, 204, 204));
        tbnUsePoint.setText("Use Point");
        tbnUsePoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tbnUsePointActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(71, 71, 71));
        jLabel12.setText("Promotion :");

        lblPromotion.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblPromotion.setForeground(new java.awt.Color(71, 71, 71));
        lblPromotion.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPromotion.setText("-");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel9))
                        .addGap(18, 18, 18))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblChange, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(lblPromotion, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblDiscount, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTotal, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                        .addComponent(txfCash, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tbnUsePoint, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(lblMemberName, javax.swing.GroupLayout.DEFAULT_SIZE, 93, Short.MAX_VALUE)
                    .addComponent(lblTel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblPoint, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblGetPoint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(133, 133, 133))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(lblMemberName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblTel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(249, 249, 249))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(12, 12, 12))
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblPromotion, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txfCash, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblGetPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblChange))
                                .addGap(40, 40, 40))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addGap(5, 5, 5)
                                        .addComponent(lblDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(lblPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(tbnUsePoint, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
        );

        btnMainMenu.setBackground(new java.awt.Color(189, 175, 150));
        btnMainMenu.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnMainMenu.setForeground(new java.awt.Color(0, 0, 0));
        btnMainMenu.setText("Back To Main Menu");
        btnMainMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMainMenuActionPerformed(evt);
            }
        });

        jPanel6.setBackground(new java.awt.Color(119, 89, 62));

        btnAll.setBackground(new java.awt.Color(207, 169, 130));
        btnAll.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnAll.setForeground(new java.awt.Color(51, 51, 51));
        btnAll.setText("All");
        btnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAllActionPerformed(evt);
            }
        });

        btnCoffee.setBackground(new java.awt.Color(157, 104, 73));
        btnCoffee.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnCoffee.setForeground(new java.awt.Color(51, 51, 51));
        btnCoffee.setText("Coffee");
        btnCoffee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCoffeeActionPerformed(evt);
            }
        });

        btnDrink.setBackground(new java.awt.Color(153, 204, 255));
        btnDrink.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnDrink.setForeground(new java.awt.Color(51, 51, 51));
        btnDrink.setText("Drink");
        btnDrink.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDrinkActionPerformed(evt);
            }
        });

        btnDessert.setBackground(new java.awt.Color(255, 204, 204));
        btnDessert.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnDessert.setForeground(new java.awt.Color(51, 51, 51));
        btnDessert.setText("Dessert");
        btnDessert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDessertActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap(39, Short.MAX_VALUE)
                .addComponent(btnAll, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(105, 105, 105)
                .addComponent(btnCoffee)
                .addGap(51, 51, 51)
                .addComponent(btnDrink)
                .addGap(34, 34, 34)
                .addComponent(btnDessert)
                .addGap(19, 19, 19))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDessert)
                    .addComponent(btnDrink)
                    .addComponent(btnCoffee)
                    .addComponent(btnAll, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel7.setBackground(new java.awt.Color(239, 216, 171));

        jLabel8.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(51, 51, 51));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("ITEM LIST");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, 41, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(btnMainMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(scrProductList, javax.swing.GroupLayout.PREFERRED_SIZE, 1130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(38, 38, 38)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 661, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 318, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnMainMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(scrProductList, javax.swing.GroupLayout.PREFERRED_SIZE, 839, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(94, 55, 16));

        jLabel2.setFont(new java.awt.Font("Yu Gothic UI", 1, 36)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("POINT OF SELL");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addContainerGap(58, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalculateActionPerformed
        if (calChange()) {
            return;
        }
        ///////////////// Set Value ////////////////
        reciept.setPoint(receivePoint);
        if (editedMember != null) {

            editedMember.setPoint(editedMember.getPoint() + (Integer.parseInt(lblGetPoint.getText())));
        }
        if (txfCash.getText().isEmpty()) {
            System.out.println("Please Set Cash");
            return;
        } else {
            reciept.setCash(Float.parseFloat(txfCash.getText()));
        }
        reciept.setTotal(reciept.getTotal() - calDiscount());

        ///////////////// Update and Insert ////////////////
        if (reciept != null) {
            System.out.println(reciept);
            if (editedMember != null) {
                memberService.update(editedMember);
            }
            reciept.setPromotionDetailId(promotionDetail.getPromotionDetailDetailId());
            recieptService.addNew(reciept);
            ReceiptDialog receiptDialog = new ReceiptDialog();
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int x = (screenSize.width - receiptDialog.getWidth()) / 2;
            int y = (screenSize.height - receiptDialog.getHeight()) / 2;
            receiptDialog.setLocation(x, y);
            receiptDialog.showReceiptDialog(reciept, lblPrice.getText(), lblMemberName.getText(), lblChange.getText(), txtName.getText(), lblGetPoint.getText(), lblPromotion.getText());

        } else {
            System.out.println("Receipt = NULL");
            return;
        }
        clearReciept();
    }//GEN-LAST:event_btnCalculateActionPerformed

    private boolean calChange() throws NumberFormatException {
        // TODO add your handling code here:
        if (!txfCash.getText().equals("")) {
            if (Float.parseFloat(txfCash.getText()) >= (reciept.getTotal() - calDiscount())) {
                float change = (Float.parseFloat(txfCash.getText()) - (reciept.getTotal() - calDiscount()));
                lblChange.setText("" + change);
            } else {
                System.out.println("Cash not enogh");
                return true;
            }
        }
        return false;
    }

    private void btnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAllActionPerformed
        // TODO add your handling code here:
        System.out.println("Clicked");
        ProductListPanel productListPanel = new ProductListPanel();
        productListPanel.addOnBuyProduct(this);
        scrProductList.setViewportView(productListPanel);
    }//GEN-LAST:event_btnAllActionPerformed

    private void btnCoffeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCoffeeActionPerformed
        // TODO add your handling code here:
        System.out.println("Coffee");
        ProductListPanel productListPanel = new ProductListPanel(1);
        productListPanel.addOnBuyProduct(this);
        scrProductList.setViewportView(productListPanel);
    }//GEN-LAST:event_btnCoffeeActionPerformed

    private void btnDrinkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDrinkActionPerformed
        // TODO add your handling code here:
        System.out.println("Drink");
        ProductListPanel productListPanel = new ProductListPanel(3);
        productListPanel.addOnBuyProduct(this);
        scrProductList.setViewportView(productListPanel);
    }//GEN-LAST:event_btnDrinkActionPerformed

    private void btnDessertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDessertActionPerformed
        // TODO add your handling code here:
        System.out.println("Dessert");
        productListPanel.refreshProductList();
        ProductListPanel productListPanel = new ProductListPanel(2);
        productListPanel.addOnBuyProduct(this);
        scrProductList.setViewportView(productListPanel);
    }//GEN-LAST:event_btnDessertActionPerformed

    private void btnMainMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMainMenuActionPerformed
        // TODO add your handling code here:
        this.mainFrame.setVisible(true);// Set the MainFrame to be visible
        // Close the PosFrame if needed
        Window w = SwingUtilities.getWindowAncestor(this);
        if (w instanceof Frame) {
            Frame frame = (Frame) w;
            frame.dispose();
        }
    }//GEN-LAST:event_btnMainMenuActionPerformed
    
    private void btnSearchMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchMemberActionPerformed
        // TODO add your handling code here:
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        SearchMemDialog searchM = new SearchMemDialog(frame);
        editedMember = searchM.showDialog();
        if (editedMember != null) {
            reciept.setMemberId(editedMember.getId());
            System.out.println("You entered: " + editedMember);
            lblMemberName.setText(editedMember.getName());
            lblTel.setText(editedMember.getTel());
            calDiscountPromotion();
            if (reciept.getTotal() != 0.0f) {
                refreshTotalDetail();
            }
        } else {
            System.out.println("no object editedMember return");
        }
        calDiscountPromotion();
    }//GEN-LAST:event_btnSearchMemberActionPerformed

    private void btnAddMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddMemberActionPerformed
        // TODO add your handling code here:
        editedMember = new Member();
        openDialog();
    }//GEN-LAST:event_btnAddMemberActionPerformed

    private void btnCancleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancleActionPerformed
        // TODO add your handling code here:
        clearReciept();


    }//GEN-LAST:event_btnCancleActionPerformed


    private void tbnUsePointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tbnUsePointActionPerformed
        // TODO add your handling code here:
        if (editedMember != null) {
            if (reciept.getTotal() >= 100 && editedMember.getPoint() >= 100) { //Condition of Discount Point
                discountPoint = editedMember.getPoint() / 10;
                lblTotal.setText("" + (reciept.getTotal() - calDiscount()));
                editedMember.setPoint(editedMember.getPoint() % 10);
                lblPoint.setText("" + editedMember.getPoint());
                refreshTotalDetail();
            } else {
                System.out.println("Point not enough");
            }
        } else {
            System.out.println("No select Member ");
        }
    }//GEN-LAST:event_tbnUsePointActionPerformed
    private float calDiscount() {
        float totalDiscount = discountPoint + discountPromotion;
        return totalDiscount;
    }

    private void refreshTotalDetail() {
        if (reciept.getTotal() >= 100 && editedMember != null) {
            receivePoint = (((int) (reciept.getTotal() / 10)) / 10) * 10;
        }
        lblGetPoint.setText("" + receivePoint);
        lblTotal.setText("" + (reciept.getTotal() - calDiscount()));
        lblPoint.setText("" + editedMember.getPoint());
        lblDiscount.setText("" + calDiscount());
    }

    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        MemberDialog memberDialog = new MemberDialog(frame, editedMember);
        memberDialog.setLocationRelativeTo(this);
        memberDialog.setVisible(true);
    }

    private void clearReciept() {
        reciept = new Receipt();
        reciept.setEmployee(EmployeeService.getCurrentUser());
        editedMember = null;
        refreshReciept();
        discountPoint = 0;
        discountPromotion = 0;
        receivePoint = 0;
        lblMemberName.setText("");
        lblTel.setText("");
        lblPoint.setText("");
        lblPrice.setText("");
        lblDiscount.setText("");
        lblTotal.setText("");
        txfCash.setText("");
        lblChange.setText("");
        lblGetPoint.setText("");
        lblPromotion.setText("");

    }

    private void closePosFrame() {
        // Check if this panel is in a JFrame (PosFrame)
        if (SwingUtilities.getWindowAncestor(this) instanceof PosFrame) {
            PosFrame posFrame = (PosFrame) SwingUtilities.getWindowAncestor(this);
            posFrame.closePosFrame();
        }
    }

    private void Time(Font tahomaFont) {
        Timer timer = new Timer(100, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                String formattedTime = LocalDateTime.now().format(formatter);
                txtDate.setText(formattedTime);
                txtDate.setForeground(Color.WHITE);
                txtDate.setFont(tahomaFont);
            }
        });
        timer.start();
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Date;
    private javax.swing.JLabel Name;
    private javax.swing.JLabel Role;
    private javax.swing.JButton btnAddMember;
    private javax.swing.JButton btnAll;
    private javax.swing.JButton btnCalculate;
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnCoffee;
    private javax.swing.JButton btnDessert;
    private javax.swing.JButton btnDrink;
    private javax.swing.JButton btnMainMenu;
    private javax.swing.JButton btnSearchMember;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblChange;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblGetPoint;
    private javax.swing.JLabel lblMemberName;
    private javax.swing.JLabel lblPoint;
    private javax.swing.JLabel lblPrice;
    private javax.swing.JLabel lblPromotion;
    private javax.swing.JLabel lblTel;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JScrollPane scrProductList;
    private javax.swing.JTable tblRecieptDetail;
    private javax.swing.JButton tbnUsePoint;
    private javax.swing.JTextField txfCash;
    private javax.swing.JLabel txtDate;
    private javax.swing.JLabel txtName;
    private javax.swing.JLabel txtRole;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Product product, int qty) {
        reciept.addReceiptDetail(product, qty);
        refreshReciept();
    }
}
