/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.softdevtermprojectdecoffee.ui;

import com.mycompany.softdevtermprojectdecoffee.model.Employee;
import com.mycompany.softdevtermprojectdecoffee.model.EmployeeRole;
import com.mycompany.softdevtermprojectdecoffee.model.ReportWorkingTime;
import com.mycompany.softdevtermprojectdecoffee.model.Salary;
import com.mycompany.softdevtermprojectdecoffee.model.WorkingTime;
import com.mycompany.softdevtermprojectdecoffee.service.EmployeeRoleService;
import com.mycompany.softdevtermprojectdecoffee.service.ReportWorkingTimeService;
import com.mycompany.softdevtermprojectdecoffee.service.SalaryService;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.ScrollBarUI;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author HP
 */
public final class PaySalary extends javax.swing.JPanel {

    /**
     * Creates new form PaySalary
     */
    private Employee employee;
    private Salary salary;
    private List<Salary> list;
    private final SalaryService salaryService = new SalaryService();
    private EmployeeRole role = new EmployeeRole();
    private EmployeeRoleService roleService = new EmployeeRoleService();
    private WorkingTime checkWork;
    private int workhour = 0;
    private double price = 0;
    private int id = 0;
    private int idrole = 0;
    
    public PaySalary(JDialog parent, Employee employee, Salary salary) {
            initComponents();

            this.employee = employee;
            this.salary = salary;
            id = employee.getId();
            forSetTextWorkhr(id);
            calPositionSalary();
            if(id > 0){
                txtAmount.setText(Double.toString(salary.getAmount()));
                txtAmount.setEditable(false);
                if(txtHourRate.getText().isEmpty()){

                }
            }else{
                txtAmount.setEditable(false);
            }

            txtHourRate.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    calAmount();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    calAmount();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    calAmount();
                }

            private void calAmount() {

                String hourRateText = txtHourRate.getText();
                if (!hourRateText.isEmpty() && !hourRateText.equals("-")) {
                try {

                    double hourRate = Double.parseDouble(hourRateText);


                    double amount = (hourRate * workhour) + price;


                    if (amount >= 0) {
                        txtAmount.setText(String.format("%.2f", amount));
                    } else {
                        txtAmount.setText("Invalid");
                    }
                } catch (NumberFormatException ex) {
                    txtAmount.setText("Invalid");
                }
            } else {
                txtAmount.setText("");
            }
        }
    });
 }

    private void calPositionSalary() {
        if (idrole == 1) {
            txtPositon.setText("1000");
            price = 1000;
        } else if (idrole == 2) {
            txtPositon.setText("800");
            price = 800;
        } else if (idrole == 3) {
            txtPositon.setText("700");
            price = 700;
        } else {
            txtPositon.setText("2000");
            price = 2000;
        }
    }

    private void forSetTextWorkhr(int id) { //getworkhr
        ReportWorkingTimeService rs = new ReportWorkingTimeService();
        LocalDate today = LocalDate.now();
        int year = today.getYear();
        int month = today.getMonthValue();
        String yearMonth = String.format("%d-%02d", year, month);
        System.out.println(yearMonth);
        ReportWorkingTime report = rs.getTotalHourByMonthAndEmp(id, yearMonth);
        if (report == null) {
            workhour = 0;
        } else {
            workhour = report.getTime_difference();
        }
        txtWorkhr.setText(Integer.toString(workhour));
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel6 = new javax.swing.JLabel();
        cmbPaymentMethod = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        txtAmount = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtPayment = new javax.swing.JLabel();
        txtPositon = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtHourRate = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtCancel = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        txtWorkhr = new javax.swing.JLabel();
        btnPaysalary = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(200, 169, 146));

        jLabel6.setText("PaymentMethod :");

        cmbPaymentMethod.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Bank", "Cash" }));

        jLabel7.setText("Amount :");

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel1.setText("Pay Salary");

        jLabel2.setText("WorkHour :");

        jLabel8.setText("Position allowance :");

        txtPositon.setText("jLabel4");

        jLabel3.setText("baht/hr");

        jLabel5.setText("Hourly rate :");

        jLabel4.setText("baht");

        txtCancel.setText("Cancel");
        txtCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCancelActionPerformed(evt);
            }
        });

        jLabel9.setText("baht");

        txtWorkhr.setText("jLabel10");

        btnPaysalary.setText("PaySalary");
        btnPaysalary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPaysalaryActionPerformed(evt);
            }
        });

        jLabel10.setText("hour");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 321, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(45, 45, 45)
                                        .addComponent(txtCancel)
                                        .addGap(32, 32, 32)
                                        .addComponent(btnPaysalary))
                                    .addComponent(txtPayment, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(txtHourRate, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel3)
                                            .addComponent(jLabel4))))
                                .addGap(24, 24, 24))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(207, 207, 207))))))
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbPaymentMethod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPositon)
                    .addComponent(txtWorkhr, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(jLabel9))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel10)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtWorkhr)
                        .addComponent(jLabel2)
                        .addComponent(jLabel10))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtHourRate, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPayment)))
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPositon)
                            .addComponent(jLabel9))
                        .addGap(34, 34, 34)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbPaymentMethod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4)))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCancel)
                    .addComponent(btnPaysalary))
                .addContainerGap(56, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    private void clearForm() {
        txtHourRate.setText("");
        txtAmount.setText("");
    }

    private void setObjectToForm() {
        forSetTextWorkhr(id);
        calPositionSalary();
        if(salary.getPaymentmethod().equals("Bank")){
            cmbPaymentMethod.setSelectedIndex(0);
        }
//        txtAmount.setText(Double.toString(salary.getAmount()));
        
    }
    private void txtCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCancelActionPerformed
        // TODO add your handling code here:
        clearForm();
    }//GEN-LAST:event_txtCancelActionPerformed

    private void btnPaysalaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPaysalaryActionPerformed
        // TODO add your handling code here:
        double amount = Double.parseDouble(txtAmount.getText());
        JFrame f=new JFrame(); 
        if(amount <= 0 || txtAmount.getText().equals("") ){
            JOptionPane.showMessageDialog(f,"Please fill out the information completely.","Alert",JOptionPane.WARNING_MESSAGE);    
        }else{
            if (salary.getId() < 0 && amount >= 0) {
                salary.setAmount(Double.parseDouble(txtAmount.getText()));
                if (cmbPaymentMethod.getSelectedIndex() == 0) {
                    salary.setPaymentmethod("Bank");
                } else {
                    salary.setPaymentmethod("Cash");
                }
                salary.setEmployeeId(employee.getId());
                salary.setEmployee(employee);
                salaryService.addNew(salary);
            }else{
                 salary.setAmount(Double.parseDouble(txtAmount.getText()));
                if (cmbPaymentMethod.getSelectedIndex() == 0) {
                    salary.setPaymentmethod("Bank");
                } else {
                    salary.setPaymentmethod("Cash");
                }
                Date currentDate = new Date();
                Timestamp timestampFromDate = new Timestamp(currentDate.getTime());
                salary.setGivetime(timestampFromDate);
                salaryService.update(salary);
                System.out.println(salary);
            }
            JOptionPane.showMessageDialog(f,"Successful.");
            
        }
    }//GEN-LAST:event_btnPaysalaryActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPaysalary;
    private javax.swing.JComboBox<String> cmbPaymentMethod;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField txtAmount;
    private javax.swing.JButton txtCancel;
    private javax.swing.JTextField txtHourRate;
    private javax.swing.JLabel txtPayment;
    private javax.swing.JLabel txtPositon;
    private javax.swing.JLabel txtWorkhr;
    // End of variables declaration//GEN-END:variables

}
